#!/bin/bash
starttime=$(date +%s)
set -e #Stop compiler on error

#Settings
flags="-ffreestanding -O2 -Wall -Wextra -isystem ./libc"

usage() {
	echo "Usage: $0 [OPTIONS]"
	echo "Options:"
	echo " -h, --help      Display this help message"
	echo " -v, --verbose   Enable verbose mode (not in use)"
	echo " -c, --rclibc    Recompile libc"
	echo " -s, --startos   Start os with qemu after compiling"
}

startos=false

while [ $# -gt 0 ]; do
	case $1 in
		-h | --help)
			usage
			exit 0
			;;
		-c | --rclibc)
			echo "--rclibc enabled, compiling libc"

			echo "Removing compiled files from /libc/"
			for file in $(find ./libc/ -type f -name "*.o")
				do rm $file
			done
			echo "Done"

			echo "Compiling all files in /libc/"
			for cfile in $(find ./libc/ -type f -name "*.c")
			do
				echo "	${cfile}"
				i686-elf-gcc $flags -c $cfile -o ${cfile::-1}o
			done
			echo "Done"
			;;
		-s | --startos)
			startos=true
			;;
		*)
			echo "Invalid option: $1" >&2
			usage
			exit 1
			;;
	esac
	shift
done

echo "Removing compiled files from /kernel/"
for ofile in $(find ./kernel -type f -name "*.o")
do
	echo "	${ofile}"
	rm $ofile
done
echo "Done"

echo "Collecting files from libc"
for clib in $(find ./libc -type f -name "*.o")
do
	echo "	${clib}"
	files=$files" "$clib
done

echo "Compiling kernel files"
for file in $(find ./kernel -not -path "./libc/*" -type f \( -name "*.S" -o -name "*.c" \))
do
	echo "	${file}"
	i686-elf-gcc $flags -c $file -o ${file::-1}o
	files=$files" "${file::-1}o
done
echo "Done"

echo "Collecting fonts"
for font in $(find ./kernel/usr/share/fonts/ -type f -name "*.sfn")
do
	echo "	${font}"
	objcopy -I binary -O elf32-i386 -B i386 $font ${font::-3}o
	files=$files" "${font::-3}o
done
echo "Done"

echo "Linking all files"
i686-elf-gcc -T ./linker.ld -o ./kernel/boot/os.bin -ffreestanding $files -nostdlib -lgcc
echo "Done"

echo "Making image file"
grub-mkrescue -o ./lymprios.iso ./kernel/
echo "Done"

endtime=$(date +%s)
runtime=$((endtime-starttime))
echo "Runtime: "$runtime"s"

if $startos == true;
then
	echo "Starting os.."
	qemu-system-i386 \
		-serial file:serial.out \
		-vga std \
		-boot d \
		-monitor stdio \
		-bios /usr/share/qemu/bios.bin \
		-cdrom lymprios.iso \
		-hda ./hdd/fat.fs \
		-fda ./floppy.img \
		2> stderr.out | tee stdout.log
fi

exit 0
