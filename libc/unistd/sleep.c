#include <unistd.h>

void sleep(unsigned int seconds) {
    // For simplicity, assume each iteration of the loop sleeps for approximately 1 second
    for (unsigned int i = 0; i < seconds; i++) {
        for (unsigned int j = 0; j < 100000000; j++) {
            // Busy waiting
        }
    }
}