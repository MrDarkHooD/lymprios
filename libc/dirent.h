#ifndef _DIRENT_H
#define _DIRENT_H

#include <stddef.h>

#define MAX_FILENAME_LENGTH 255

struct dirent {
	char d_name[MAX_FILENAME_LENGTH];
};

typedef struct {
	// Directory data structure (e.g., pointer to directory entry array)
	struct dirent entries[10]; // Example: 10 entries max
	size_t num_entries;
	size_t current_index;
} DIR;

DIR *opendir(const char *dirname);
struct dirent *readdir(DIR *dirp);
int closedir(DIR *dirp);

#endif /* _DIRENT_H */
