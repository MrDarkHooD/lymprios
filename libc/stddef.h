#ifndef _STDDEF_H
#define _STDDEF_H

typedef unsigned long size_t;
typedef long ssize_t;
typedef long ptrdiff_t;
typedef int pid_t;

#define NULL ((void *)0)

typedef struct stack_t Stack;

#endif /* _STDDEF_H */
