#ifndef _STDLIB_H
#define _STDLIB_H

#include <stddef.h>
#include <sys/sys.h>

#define RAND_MAX 32767

typedef struct {
	size_t size;   // Size of the allocated memory block in bytes
	void *data;    // Pointer to the allocated memory block
} MemoryBlock;

typedef struct {
    long int quot;
    long int rem;
} div_t;

typedef struct {
    int quot;
    int rem;
} ldiv_t;

int atoi(const char *p);

void free(void *ptr);
int malloc(size_t size);

int abs(int i);
div_t div(int numer, int denom);
long int labs(long int x);
ldiv_t ldiv(long int numer, long int denom);
int rand(void);
void srand(unsigned int seed);

void itoa (char *buf, int base, int d);

#endif
