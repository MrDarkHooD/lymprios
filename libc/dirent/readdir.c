#include <dirent.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct dirent *readdir(DIR *dirp) {
  
	if (dirp == NULL) {
		int errno = EBADF; // Bad file descriptor
		return NULL;
	}
	
	if (dirp->current_index >= dirp->num_entries) {
		return NULL; // No more entries or invalid directory
	}

	// Read the next directory entry
	struct dirent *entry = &dirp->entries[dirp->current_index];
	dirp->current_index++;
	
	//if (entry == NULL) {
		//int errno = ENOMEM; // Out of memory
		//return NULL;
//	}

	// Simulate reading a directory entry with dummy
//	strcpy(entry->d_name, "example.txt");

	return entry;
}
