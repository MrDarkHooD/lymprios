#include <stddef.h>
int round(double x)
{
    if (x < 0.0)
        return (int)(x - 0.5);
    else
        return (int)(x + 0.5);
}

int abs (int i)
{
  return i < 0 ? -i : i;
}