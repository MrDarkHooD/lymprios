#include <stdlib.h>

ldiv_t ldiv(long int number, long int denom) {
	ldiv_t output = {
		.quot = number / denom,
		.rem = number - (number / denom)
	};
	
	return output;
}
