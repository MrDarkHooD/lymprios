//https://github.com/haltode/yaos/blob/master/libc/stdlib/malloc.c
#include <stddef.h>
#include <sys/sys.h>
int malloc(size_t size)
{
	return syscall(MALLOC_SYSCALL, (int) size, 0, 0);
}