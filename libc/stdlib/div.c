#include <stdlib.h>

div_t div(int number, int denom) {
	div_t output = {
		.quot = number / denom,
		.rem = number - (number / denom)
	};
	
	return output;
}
