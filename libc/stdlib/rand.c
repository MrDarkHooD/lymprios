#include <string.h>
#include <stdlib.h>

unsigned int random_seed;

int rand(void)
{
	random_seed = random_seed * 1103515245 + 12345;
	return (unsigned int)(random_seed / 65536) % (RAND_MAX+1); 
}
