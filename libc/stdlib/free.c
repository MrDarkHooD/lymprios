#include <stdlib.h>
void free(void *ptr)
{
   syscall(FREE_SYSCALL, (int) ptr, 0, 0);
}
