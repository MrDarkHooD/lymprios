int atoi(const char *p) {
	int n = 0;
	while(*p) n = (n * 10) + (*p++ - '0');
	return n;
}