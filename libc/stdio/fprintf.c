#include <stdarg.h>
#include <stddef.h>
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <sys/io.h>

int fprintf(FILE *stream, const char *format, ...) {
	va_list args;
	va_start(args, format);
	char buffer[1024];

	vsnprintf(buffer, sizeof(buffer), format, args);

	// Output the formatted string
	for (const char* p = buffer; *p != '\0'; ++p)
		outb((uint16_t)stream, *p);

	va_end(args);
	return 0;
}
