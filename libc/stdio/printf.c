#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <sys/io.h>

void printf(const char *format, ...) {
	va_list args;
	va_start(args, format);
	char buffer[1024];

	vsnprintf(buffer, sizeof(buffer), format, args);

	// Output the formatted string
	for (const char* p = buffer; *p != '\0'; ++p)
		putchar(*p);

	va_end(args);
	return;
}
