#include <stddef.h>
#include <stdint.h>

/* The number of columns. */
#define COLUMNS                 80
/* The number of lines. */
#define LINES                   24
/* The attribute of an character. */
#define ATTRIBUTE               7
/* The video memory address. */
#define VIDEO                   0xB8000

#define VGA_CTRL_PORT 0x3D4
#define VGA_DATA_PORT 0x3D5
#define VGA_CURSOR_HIGH 14
#define VGA_CURSOR_LOW 15

int xpos;
int ypos;

volatile unsigned char *video = (unsigned char *)VIDEO;

void update_cursor_position()
{
	// Calculate the position of the cursor on the screen
	uint16_t position = ypos * COLUMNS + xpos;

	// Write the high byte of the cursor position to the VGA controller
	outb(VGA_CTRL_PORT, VGA_CURSOR_HIGH);
	outb(VGA_DATA_PORT, (uint8_t)(position >> 8));

	// Write the low byte of the cursor position to the VGA controller
	outb(VGA_CTRL_PORT, VGA_CURSOR_LOW);
	outb(VGA_DATA_PORT, (uint8_t)(position & 0xFF));
}

void putchar_nl()
{
	if (ypos >= LINES - 1)
	{
		// Scroll up by copying each row to the row above it
		for (size_t y = 1; y < LINES; y++) {
			for (size_t x = 0; x < COLUMNS; x++) {
				size_t src_index = y * COLUMNS + x;
				size_t dest_index = (y - 1) * COLUMNS + x;
				video[dest_index * 2] = video[src_index * 2];
				video[dest_index * 2 + 1] = video[src_index * 2 + 1];
			}
		}
		// Clear the last row
		size_t last_row_index = (LINES - 1) * COLUMNS;
		for (size_t x = 0; x < COLUMNS; x++) {
			video[last_row_index * 2 + x * 2] = ' ';
			video[last_row_index * 2 + x * 2 + 1] = ATTRIBUTE;
		}
		ypos = LINES - 1;
	} else {
		ypos++;
	}
	xpos = 0;
}

void putchar (int c)
{
	if (c == '\n' || c == '\r')
	{
		putchar_nl();
	} else if (c == '\b') {
		// Handle backspace character
		if (xpos > 0)
		{
			xpos--; // Move cursor back one position
		} else {
			if (ypos > 0) {
				ypos--; // Move cursor up one line
				xpos = COLUMNS - 1; // Move cursor to the end of the previous line
			}
		}
		// Clear character at new cursor position
		video[(xpos + ypos * COLUMNS) * 2] = ' ';
		video[(xpos + ypos * COLUMNS) * 2 + 1] = ATTRIBUTE;
	} else {

		video[(xpos + ypos * COLUMNS) * 2] = c & 0xFF;
		video[(xpos + ypos * COLUMNS) * 2 + 1] = ATTRIBUTE;

		xpos++;
		if (xpos >= COLUMNS) {
			xpos = 0;
			ypos++;
			if (ypos >= LINES) {
				putchar_nl();
			}
		}
	}
	update_cursor_position();
}
