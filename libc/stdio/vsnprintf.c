#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#include <ctype.h>
#include <float.h>

int vsnprintf(char *str, size_t size, const char *format, va_list args) {
	long unsigned int written = 0;
	int length = 0;
	int c;

	while ((c = *format++) != 0 && written < size - 1) {

		if (c != '%') {
			str[written++] = c;
			continue;
		}

		c = *format++; // Skip the '%'
		if (c == '\0') break; // End of format string

		char *p, *p2;
		int pad0 = 0, pad = 0;

		if (c == '0')
		{
			pad0 = 1;
			c = *format++;
			if (c >= '0' && c <= '9')
			{
				pad = c - '0';
				c = *format++;
			}
		}

		switch (c)
		{
			case 'c': { // Character
				int value = va_arg(args, int);
				str[written++] = (char)value;

				break;
			}

			case 'd': // Signed decimal integer
			case 'i': { // Signed decimal integer
				int value = va_arg(args, int);
				if (length < 0 || (unsigned int)(written + length) >= size) {
					fprintf(stderr, "Buffer overflow");
					errno = ENOBUFS;
					return -1;
				}

				char buff[20];
				itoa(buff, 10, value);
				p = buff;

				for (p2 = p; *p2; p2++);
				for (; p2 < p + pad; p2++) {
					str[written++] = pad0 ? '0' : ' ';
				}
				while (*p) {
					str[written++] = *p++;
				}

				break;
			}

			case 'e': // Scientific notation (mantissa/exponent) using e character

				break;

			case 'E': // Scientific notation (mantissa/exponent) using E character

				break;

			case 'f': { // Decimal floating point
				double value = va_arg(args, double);
				if (length < 0 || written + length >= size) {
					fprintf(stderr, "Buffer overflow");
					errno = ENOBUFS;
					return -1;
				}

				// NTM: gcvt()


				char buff[20];
				itoa(buff, 10, value);
				p = buff;

				break;
			}

			case 'g': // Uses the shorter of %e or %f.

				break;

			case 'G': // Uses the shorter of %E or %f

				break;

			case 'o': { // Signed octal
				// Fetch the next integer argument
				unsigned int value = va_arg(args, unsigned int);
				if (length < 0 || written + length >= size) {
					fprintf(stderr, "Buffer overflow");
					errno = ENOBUFS;
					return -1;
				}

				char buff[20];
				itoa(buff, 8, value);
				p = buff;

				for (p2 = p; *p2; p2++);
				for (; p2 < p + pad; p2++) {
					str[written++] = pad0 ? '0' : ' ';
				}
				while (*p) {
					str[written++] = *p++;
				}

				break;
			}

			case 's': { // String of characters
				char *value = va_arg(args, char *);
				while (*value != '\0' && written < size - 1) {
					str[written++] = *value++;
				}

				break;
			}

			case 'u': { // Unsigned decimal integer
				unsigned int value = va_arg(args, unsigned int);
				if (length < 0 || written + length >= size) {
					fprintf(stderr, "Buffer overflow");
					errno = ENOBUFS;
					return -1;
				}

				char buff[256];
				itoa(buff, 10, value);
				p = buff;

				for (p2 = p; *p2; p2++);
				for (; p2 < p + pad; p2++) {
					str[written++] = pad0 ? '0' : ' ';
				}
				while (*p) {
					str[written++] = *p++;
				}

				break;
			}

			case 'x': // Unsigned hexadecimal integer
			case 'X': { // Unsigned hexadecimal integer with uppercase letters
				int value = va_arg(args, int);

				if (length < 0 || written + length >= size) {
					fprintf(stderr, "Buffer overflow");
					errno = ENOBUFS;
					return -1;
				}

				char buff[20];
				itoa(buff, 16, value);
				p = buff;

				for (p2 = p; *p2; p2++);
				for (; p2 < p + pad; p2++) {
					str[written++] = pad0 ? '0' : ' ';
				}

				while (*p) {
					if(*format == 'X')
						str[written++] = toupper(*p++);
					else
						str[written++] = *p++;
				}

				break;
			}

			case 'p': // Pointer address

				break;

			case 'n': // Nothing printed

				break;

			default: {
				// Unsupported specifier, just copy it as is
				if ((unsigned int)written < size - 1) {
					str[written++] = '%';
					str[written++] = *format;
				} else {
					// Buffer space exhausted, truncate and terminate the string
					// This should be made as global check
					errno = ENOBUFS;
					written = size - 1;
					str[written] = '\0';
				}
				break;
			}
		}
	}

	str[written] = '\0';

	return written;
}
