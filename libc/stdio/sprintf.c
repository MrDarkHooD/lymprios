#include <stdarg.h>
#include <stdio.h>
#include <string.h>
#include <stddef.h>
#include <string.h>

int sprintf(char *str, const char *format, ...)
{
    va_list args;
    va_start(args, format);
    
    int written = vsnprintf(str, 255, format, args);
    
    va_end(args);
    
    return written;
}

int sprintf_integer(char *str, size_t size, int value)
{
    // Use snprintf to safely write the formatted string into the buffer
    int written = snprintf(str, size, "%d", value);
    
    // Check if the value was successfully formatted and written to the buffer
    if (written < 0 || written >= (int)size) {
        fprintf(stderr, "Failed to format and write value to buffer.");
        return -1;  // or any error indicator
    }
    
    // Return the length of the resulting string
    return written;
}

int sprintf_string(char *str, size_t size, const char *value)
{
    // Copy the string value into the destination buffer
    size_t len = strlen(value);
    if (len < size) {
        strcpy(str, value);
        return len;
    } else {
        // Insufficient buffer space, truncate the string
        strncpy(str, value, size - 1);
        str[size - 1] = '\0';
        return size - 1;
    }
}