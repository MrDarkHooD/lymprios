#include <stdarg.h>
#include <stdio.h>
#include <stddef.h>

#define BUFFER_SIZE 255

int snprintf(char *str, const char *format, ...)
{
	va_list args;
	va_start(args, format);

	int written = vsnprintf(str, BUFFER_SIZE, format, args);

	va_end(args);

	return written;
}