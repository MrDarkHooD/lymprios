#include <stddef.h>
#include <time.h>

time_t mktime(struct tm *timeptr) {
  // Some error checking
  if (timeptr == NULL) {
    return (time_t)-1;
  }

  // Make sure the fields of the struct tm are within valid ranges
  if (timeptr->tm_sec < 0 || timeptr->tm_sec > 59 ||
    timeptr->tm_min < 0 || timeptr->tm_min > 59 ||
    timeptr->tm_hour < 0 || timeptr->tm_hour > 23 ||
    timeptr->tm_mday < 1 || timeptr->tm_mday > 31 ||
    timeptr->tm_mon < 0 || timeptr->tm_mon > 11 ||
    timeptr->tm_year < 0 || timeptr->tm_year > 1900) {
    return (time_t)-1; // Return an error value
  }

  // Adjust month and year for mktime
  timeptr->tm_mon -= 1;
  timeptr->tm_year -= 1900;

  // Convert struct tm to time_t
  time_t result = 0;

  // Adjust the time zone offset
  result += timeptr->tm_sec;
  result += timeptr->tm_min * 60;
  result += timeptr->tm_hour * 3600;
  result += (timeptr->tm_mday - 1) * 86400;
  result += timeptr->tm_mon * 2629743; // Average number of seconds in a month
  result += ((timeptr->tm_year - 70) * 31536000); // Average number of seconds in a year
  result += ((timeptr->tm_year - 69) / 4) * 86400; // Add leap year seconds
  result -= ((timeptr->tm_year - 1) / 100 - (timeptr->tm_year - 1) / 400) * 86400; // Subtract century years
  result -= TIMEZONE * 3600; // Adjust for local time zone (1 hour = 3600 seconds)
  result += timeptr->tm_isdst * 3600; // Adjust for daylight saving time
  
  // Adjust month and year back
  timeptr->tm_mon += 1;
  timeptr->tm_year += 1900;

  return result;
}
//1714615599