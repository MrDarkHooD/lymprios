#include <stdio.h>
#include <string.h>
#include <time.h>

#include "../../kernel/drivers/cmos/cmos.h"

/*
 * Return current time in seconds since the epoch (January 1, 1970 00:00:00 UTC+0)
 * Time comes from CMOS rtc
*/

void time(time_t *buffer) {

	unsigned int second  = get_rtc_second();
	unsigned int minute  = get_rtc_minute();
	unsigned int hour    = get_rtc_hour();
	unsigned int day     = get_rtc_day();
	unsigned int month   = get_rtc_month();
	unsigned int year    = get_rtc_year();
	unsigned int wday    = get_rtc_weekday();
	unsigned int isdst   = get_rtc_isdst();

	struct tm rtc_time = {0};
	rtc_time.tm_sec   = second;
	rtc_time.tm_min   = minute;
	rtc_time.tm_hour  = hour;
	rtc_time.tm_mday  = day;
	rtc_time.tm_mon   = month - 1;
	rtc_time.tm_year  = year - 1900;
	rtc_time.tm_wday  = wday;
	rtc_time.tm_yday  = 0;
	rtc_time.tm_isdst = isdst;

	//long epoch_time = mktime(&rtc_time);

	*buffer = mktime(&rtc_time);
}
