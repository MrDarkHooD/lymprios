#include <stddef.h>
#include <time.h>

struct tm *localtime(const time_t *timep) {
    static struct tm tm_result; // Static variable to store the result
    time_t t = *timep;
    int leap_year, day_of_year, month, year, day_of_month;

    // Number of days in each month (non-leap year)
    static const int days_in_month[] = {
        31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31
    };

    // Number of days in each month (leap year)
    static const int days_in_month_leap[] = {
        31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31
    };

    // Seconds in a day
    static const int seconds_in_day = 24 * 60 * 60;

    // Offset for local time (for example, UTC+1 is 3600 seconds)
    static const int timezone_offset = 0; // Adjust this value according to your timezone

    // Days since epoch (January 1, 1970) to the input time
    int days_since_epoch = t / seconds_in_day;

    // Number of seconds into the day
    int seconds_into_day = t % seconds_in_day;

    // Day of the week (0 = Sunday, 1 = Monday, ..., 6 = Saturday)
    tm_result.tm_wday = (days_since_epoch + 4) % 7;

    // Year
    year = 1970;
    while (days_since_epoch >= 365) {
        leap_year = (year % 4 == 0 && (year % 100 != 0 || year % 400 == 0));
        day_of_year = leap_year ? 366 : 365;
        if (days_since_epoch >= day_of_year) {
            days_since_epoch -= day_of_year;
            year++;
        } else {
            break;
        }
    }
    tm_result.tm_year = year - 1900;

    // Month and day of the month
    leap_year = (year % 4 == 0 && (year % 100 != 0 || year % 400 == 0));
    const int *days_in_month_array = leap_year ? days_in_month_leap : days_in_month;
    for (month = 0; month < 12; month++) {
        day_of_month = days_in_month_array[month];
        if (days_since_epoch >= day_of_month) {
            days_since_epoch -= day_of_month;
        } else {
            break;
        }
    }
    tm_result.tm_mon = month;
    tm_result.tm_mday = days_since_epoch + 1;

    // Hours, minutes, and seconds
    tm_result.tm_hour = seconds_into_day / 3600;
    tm_result.tm_min = (seconds_into_day / 60) % 60;
    tm_result.tm_sec = seconds_into_day % 60;

    // Adjust for local timezone
    tm_result.tm_sec += timezone_offset;
    if (tm_result.tm_sec < 0) {
        tm_result.tm_sec += 60;
        tm_result.tm_min--;
    }
    if (tm_result.tm_sec >= 60) {
        tm_result.tm_sec -= 60;
        tm_result.tm_min++;
    }
    if (tm_result.tm_min < 0) {
        tm_result.tm_min += 60;
        tm_result.tm_hour--;
    }
    if (tm_result.tm_min >= 60) {
        tm_result.tm_min -= 60;
        tm_result.tm_hour++;
    }
    if (tm_result.tm_hour < 0) {
        tm_result.tm_hour += 24;
        tm_result.tm_mday--;
    }
    if (tm_result.tm_hour >= 24) {
        tm_result.tm_hour -= 24;
        tm_result.tm_mday++;
    }

    return &tm_result;
}