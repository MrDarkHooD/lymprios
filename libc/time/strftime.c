#include <stddef.h>
#include <stdio.h>
#include <string.h>
#include <time.h>

#include "../../kernel/drivers/cmos/cmos.h"

#define TIMEZONE "Europe/Helsinki"

int strftime(char *str, size_t maxsize, const char *format, struct tm *timeptr)
{
	int written = 0;
	while (*format != '\0' && written < maxsize - 1) {
		if (*format == '%') {
			format++; // Skip the '%'
			if (*format == '\0') break; // End of format string
			
			switch (*format) {
				case 'a': { // Abbreviated weekday name
					strncpy(str + written, "%a", maxsize - written);
					written += 2;
					break;
				}
				case 'A': { // Full weekday name
					strncpy(str + written, "%A", maxsize - written);
					written += 2;
					break;
				}
				case 'b': { // Abbreviated month name
					strncpy(str + written, "%b", maxsize - written);
					written += 2;
					break;
				}
				case 'B': { // Full month name
					strncpy(str + written, "%B", maxsize - written);
					written += 2;
					break;
				}
				case 'c': { // Date and time representation (Sun Aug 19 02:56:02 2012)
					strncpy(str + written, "%c", maxsize - written);
					written += 2;
					break;
				}
				case 'd': { // Day of the month (01-31)
					written += snprintf(str + written, maxsize - written, "%d", timeptr->tm_mday);
					break;
				}
					
				case 'H': { // Hour in 24h format (00-23)
					written += snprintf(str + written, maxsize - written, "%d", timeptr->tm_hour);
					break;
				}
				case 'I': { // Hour in 12h format (01-12)
					written += snprintf(str + written, maxsize - written, "%d", (timeptr->tm_hour % 12 == 0) ? 12 : (timeptr->tm_hour % 12));
					break;
				}
				case 'j': { // Day of the year (001-366)
					strncpy(str + written, "%j", maxsize - written);
					written += 2;
					break;
				}
				case 'm': { // Month as a decimal number (01-12)
					written += snprintf(str + written, maxsize - written, "%d", (timeptr->tm_mon + 1));
					break;
				}
				case 'M': { // Minute (00-59)
					written += snprintf(str + written, maxsize - written, "%d", timeptr->tm_min);
					break;
				}
				case 'p': { // AM or PM designation
					written += snprintf(str + written, maxsize - written, "%s", (timeptr->tm_hour >= 12) ? "PM" : "AM");
					break;
				}
				case 'S': { // Second (00-61)
					written += snprintf(str + written, maxsize - written, "%02d", timeptr->tm_sec);
					break;
				}
				case 'U': { // Week number with the first Sunday as the first day of week one (00-53)
					strncpy(str + written, "%U", maxsize - written);
					written += 2;
					break;
				}
				case 'w': { // Weekday as a decimal number with Sunday as 0 (0-6)
					int value = get_rtc_weekday();
					written += snprintf(str + written, maxsize - written, "%d", timeptr->tm_wday);
					break;
				}
				case 'W': { // Week number with the first Monday as the first day of week one (00-53)
					strncpy(str + written, "%W", maxsize - written);
					written += 2;
					break;
				}
				case 'x': { // Date representation (m/d/y)
					//int day = timeptr->tm_mday;
					//int month = timeptr->tm_mon;
					//int year = timeptr->tm_wday;
					strncpy(str + written, "%x", maxsize - written);
					written += 2;
					break;
				}
				case 'X': { // Time representation (H:M:S)
					//int second = timeptr->tm_sec;
					//int minute = timeptr->tm_min;
					//int hour = timeptr->tm_hour;
					strncpy(str + written, "%X", maxsize - written);
					written += 2;
					break;
				}
				case 'y': { // Year, last two digits (00-99)
					//int year = get_rtc_year();
					//year = (year & 0xFF);
					strncpy(str + written, "%y", maxsize - written);
					//written += snprintf(str + written, maxsize - written, "%s", "%y");
					written += 2;
					break;
				}
				case 'Y': { // Year
					written += snprintf(str + written, maxsize - written, "%d", (timeptr->tm_year + 1900));
					break;
				}
				case 'Z': { // Timezone name or abbreviation
					strncpy(str + written, TIMEZONE, maxsize - written);
					written += strlen(TIMEZONE);
					break;
				}
				case '%': { // A % sign
					strncpy(str + written, "%", maxsize - written);
					written++;
					break;
				}
					
				default:
					// Unsupported specifier, just copy as is
					if (written < maxsize - 1) {
						str[written++] = '%';
						str[written++] = *format;
					} else {
						fprintf(stderr, "Buffer space exhausted");
					}
					break;
				
			}
		} else {
			// Regular character, just copy it
			str[written++] = *format;
		}
		format++;
	}
	str[written] = '\0';

	return written;
}
