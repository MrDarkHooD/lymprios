#ifndef STDIO_H
#define STDIO_H

#include <stdint.h>
#include <stddef.h>

/*
 * This file has copypasted list of all standard
 * libc functions in stdio. All commented functions
 * have not been yet implemented, they are placeholders.
*/

#define EOF (-1)

typedef struct {
	char buffer[1024];   // Example buffer size
	size_t pos;          // Current position in the buffer
	int fd;              // File descriptor
	int eof;             // End Of File
} FILE;

extern FILE *stdin;
extern FILE *stdout __attribute__((weak)); //COM1, must be weak because defined in kernel, used in libc
extern FILE *stderr __attribute__((weak)); //COM2,  must be weak because defined in kernel, used in lib
extern FILE *stdreboot; //COM3 don't use, reboot trigger
extern FILE *stdvoid; //COM4
extern FILE *stdfree; //COM5

//clearerr()
//clrmemf()
//fclose()
//fdelrec()
int feof(FILE *stream);
//ferror()
//fflush()
//fgetc()
//fgetpos()
//fgets()
//fldata()
//flocate()
//fopen()
int fprintf(FILE *stream, const char *format, ...);
int fputc(int c, FILE *stream);
//fputs()
//fread()
//freopen()
//fscanf()
//fseek()
//fseeko()
//fsetpos()
//ftell()
//ftello()
//fupdate()
size_t fwrite(const void *ptr, size_t size, size_t count, FILE *stream);
//getc()
//getchar()
//gets()
void perror(const char *s);
void printf (const char *format, ...);
//putc()
void putchar (int c);
//puts()
//remove()
//rename()
//rewind()
//scanf()
//setbuf()
//setvbuf()
int sprintf(char *str, const char *format, ...);
//sscanf()
//svc99()
//tmpfile()
//tmpnam()
//ungetc()
//vfprintf()
//int vsprintf(char *str, const char *format, va_list args);
int vsprintf(char *str, size_t size, const char *format, va_list args);
int vsnprintf(char *str, size_t size, const char *format, va_list args);
int snprintf(char *str, const char *format, ...);

// These should not be here.
int sprintf_integer(char *str, size_t size, int value);
int sprintf_string(char *str, size_t size, const char *value);

#endif
