/*
* Copyright (c) 2019 Hugo Hurskainen

* Permission is hereby granted, free of charge, to any person obtaining
* a copy of this software and associated documentation files (the
* "Software"), to deal in the Software without restriction, including
* without limitation the rights to use, copy, modify, merge, publish,
* distribute, sublicense, and/or sell copies of the Software, and to
* permit persons to whom the Software is furnished to do so, subject to
* the following conditions:
* 
* The above copyright notice and this permission notice shall be included
* in all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
* CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
* TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
* SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
#include <string.h>

uint32_t longToString (char *buf, uint32_t bufLen, const uint8_t base, uint64_t u) {
  uint64_t c = u;
  uint8_t digit;

  for(uint32_t i = bufLen - 1; i>0; i--){
    digit = c % base;
    char d = '0' + digit;
    if(d > '9'){
      d += 7;
    }
    buf[i] = d;
    c = c / base;
  }

  buf[0] = '0';

  uint32_t startIndex = 0;
  for(uint32_t i = 0; i<bufLen; i++){
    if(buf[i] != '0'){
        startIndex = i;
        break;
    }
  }

  uint32_t to = 0;
  for(uint32_t i = startIndex; i<bufLen; i++){
    buf[to] = buf[i];
    to++;
  }
  buf[to] = '\0';
  
  return to;
}