#ifndef _ERRNO_H
#define _ERRNO_H

extern int errno;

// Error codes
#define EDOM    1   /* Domain error */
#define ERANGE  2   /* Range error */
#define EINVAL  3   /* Invalid value error */
#define ENOMEM  4   /* Out of memory */
#define EIO     5   /* Input output error */
#define EBADF   6   /* Bad file descriptor */

// Add more error codes as needed

#endif /* _ERRNO_H */
