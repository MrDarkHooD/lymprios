#ifndef _TIME_H
#define _TIME_H

typedef long time_t;

struct tm {
  int tm_sec;   // seconds after the minute [0, 61]
  int tm_min;   // minutes after the hour [0, 59]
  int tm_hour;  // hours since midnight [0, 23]
  int tm_mday;  // day of the month [1, 31]
  int tm_mon;   // months since January [0, 11]
  int tm_year;  // years since 1900
  int tm_wday;  // days since Sunday [0, 6]
  int tm_yday;  // days since January 1 [0, 365]
  int tm_isdst; // Daylight Saving Time flag
};

void time(time_t *buffer);
time_t mktime(struct tm *timeptr);
int strftime(char *str, size_t maxsize, const char *format, struct tm *timeptr);
struct tm *localtime(const time_t *timep);

// Define symbolic constants for time zones
#define TIMEZONE_HAWAII    (-10) // Hawaii Standard Time
#define TIMEZONE_ALASKA    (-9)  // Alaska Standard Time
#define TIMEZONE_PACIFIC   (-8)  // Pacific Standard Time
#define TIMEZONE_MOUNTAIN  (-7)  // Mountain Standard Time
#define TIMEZONE_CENTRAL   (-6)  // Central Standard Time
#define TIMEZONE_EASTERN   (-5)  // Eastern Standard Time
#define TIMEZONE_GMT       (0)   // Greenwich Mean Time
#define TIMEZONE_CET       (1)   // Central European Time
#define TIMEZONE_EET       (2)   // Eastern European Time
#define TIMEZONE_EEST      (3)   // Eastern European Summer Time

#define DEFAULT_TIMEZONE TIMEZONE_GMT
#define TIMEZONE TIMEZONE_EEST

#endif