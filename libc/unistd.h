#ifndef _UNISTD_H
#define _UNISTD_H

#include <stddef.h>
#include <stdint.h>

// File operations
extern ssize_t read(int fd, void *buf, size_t count);
extern ssize_t write(int fd, const void *buf, size_t count);

// Process operations
extern pid_t fork(void);
extern int execve(const char *filename, char *const argv[], char *const envp[]);
extern int getpid(void);

void sleep(unsigned int seconds);
  
#endif /* _UNISTD_H */
