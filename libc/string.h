#ifndef STRING_H
#define STRING_H
#include <stddef.h>
#include <stdint.h>

/*
 * This file has copypasted list of all standard
 * libc functions in string.h. All commented functions
 * have not been yet implemented, they are placeholders.
*/

size_t strlen(const char* str);
char *strcpy(char *dest, const char *src);
char *strncpy(char *dest, const char *src, size_t n);
char *strcat(char *dest, const char *src);
//strncat()
int strcmp(const char * cs,const char * ct);
int strncmp(const char *s1, const char *s2, size_t n);
char *strchr(const char *s, int c);
//strrchr()
char *strsep(char **stringp, const char *delim);
//strstr()
//strcspn()
//strspn()
char *strpbrk(const char *s1, const char *s2);char *strpbrk(const char *s1, const char *s2);
char *strtok(char *s, const char *delim);
char *strtok_r(char *s, const char *delim, char **holder);
//strcoll()
void *memset(void *ptr, int value, size_t size);
//memcmp()
void* memcpy(void* restrict dstptr, const void* restrict srcptr, size_t size);
void* memmove(void* dstptr, const void* srcptr, size_t size);
//memchr()

// These should not be here
uint32_t longToString (char *buf, uint32_t bufLen, const uint8_t base, uint64_t u);

#endif