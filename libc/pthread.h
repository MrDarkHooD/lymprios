#ifndef PTHREAD_H
#define PTHREAD_H

#include <stddef.h> // Include stddef.h for size_t
#include <stdint.h> // Include stdint.h for intptr_t


typedef void *(*pthread_func_t)(void *);

typedef struct {
    int dummy; // Placeholder, actual attributes depend on the platform
  void *(*start_routine)(void *);
  void *arg;
} pthread_attr_t;

typedef intptr_t pthread_t;

typedef struct {
    pthread_func_t func; // Thread function pointer
    void *arg;           // Thread function argument
} pthread_tcb_t;

void *start_routine(void *arg);
int pthread_create(pthread_t *thread, const pthread_attr_t *attr,
                   void *(*start_routine)(void *), void *arg);
int pthread_join(pthread_t thread, void **value_ptr);

#endif