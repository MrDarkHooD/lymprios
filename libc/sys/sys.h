#ifndef _SYS_H
#define _SYS_H

#include <sys/types.h>

enum syscall_t {
   PUTCHAR_SYSCALL,
   GETCHAR_SYSCALL,

   MALLOC_SYSCALL,
   FREE_SYSCALL,
   CALLOC_SYSCALL,

   OPEN_SYSCALL,
   CLOSE_SYSCALL,
   READ_SYSCALL,
   WRITE_SYSCALL,
   READ_DIR_SYSCALL,
   FIND_DIR_SYSCALL,

   ABORT_SYSCALL
};

void sys_halt(void);
void sys_enable_interrupts(void);
void sys_disable_interrupts(void);
long syscall(long number, ...);
pid_t fork(void);
pid_t clone(int flags, void *child_stack, int *ptid, int *ctid, unsigned long newtls);
void exit(int status);

int execve(const char *filename, char *const argv[], char *const envp[]);

#endif
