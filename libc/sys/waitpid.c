#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

pid_t waitpid(pid_t pid, int *status, int options) {
    // For simplicity, assume we only have one child process
    if (pid == -1) {
        // Wait for any child process
        pid = wait(status);
    } else {
        // Wait for a specific child process
        pid = waitpid(pid, status, options);
    }
    
    return pid;
}