#ifndef _SYS_SYSCALL_H
#define _SYS_SYSCALL_H

long syscall(long number, ...);
  
#endif /* _SYS_SYSCALL_H */