#include <stdio.h>
#include <stddef.h>

void insl (unsigned short int port, void *addr, unsigned long int count)
{
  asm volatile("cld; rep; insl"
               : "=D"(addr), "=c"(count)
               : "d"(port), "0"(addr), "1"(count));
}