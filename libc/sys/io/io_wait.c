#include <stdio.h>
#include <stddef.h>
#include <sys/io.h>

void io_wait (void)
{
	/* Port 0x80 is used for 'checkpoints' during POST. */
	outb(0x80, 0);
	/* TODO: does the register need to be zeroed? */
}
