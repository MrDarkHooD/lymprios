#include <stdio.h>
#include <stddef.h>

void outl (unsigned int value, unsigned short int port)
{
  asm volatile ("outl %0,%w1": :"a" (value), "Nd" (port));
}