#include <sys/sys.h>
#include <sys/types.h>
#include <stddef.h>
#include <stdio.h>
#include <string.h>

pid_t clone(int flags, void *child_stack, int *ptid, int *ctid, unsigned long newtls) {
	// For simplicity, this implementation only supports CLONE_VM flag
	if (flags & CLONE_VM) {
			pid_t pid = fork(); // Fork a new process
			if (pid == -1) {
				perror("fork");
				printf(stderr, "fork error\n");
				return -1;
			} else if (pid == 0) {
				// Child process
				// Execute the cloned function in the child process
				if (child_stack != NULL) {
					void (*fn)(void) = (void (*)(void))child_stack;
					fn(); // Execute the function
					exit(0); // Terminate the child process
				}
			} else {
				// Parent process
				// Return the PID of the child process
				if (ctid != NULL) {
					*ctid = pid;
				}
				return pid;
			}
	} else {
		// Unsupported flags
		//fprintf(stderr, "Unsupported flags\n");
		printf("Unsupported flags\n");
			return -1;
	}
}