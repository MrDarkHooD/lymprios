#include <sys/types.h>
#include <sys/wait.h>
#include <sys/sys.h>
#include <unistd.h>

pid_t wait(int *status) {
	// For simplicity, assume we only have one child process
	return waitpid(-1, status, 0);
}