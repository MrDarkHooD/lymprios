#include <unistd.h>
#include <sys/types.h>
#include <sys/sys.h>

pid_t fork(void) {
	// Create a new process
	pid_t pid = clone(CLONE_VM | CLONE_FS | CLONE_FILES | CLONE_SIGHAND | CLONE_THREAD, NULL, NULL, NULL, NULL);
    
	// Check if fork succeeded
	if (pid < 0) {
		// Fork failed, return -1
		return -1;
	} else if (pid == 0) {
		// Child process, return 0
		return 0;
	} else {
		// Parent process, return child's process ID
		return pid;
	}
}