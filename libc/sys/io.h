#ifndef _SYS_IO_H
#define _SYS_IO_H

uint8_t inb (uint16_t port);

void outb (uint16_t port, uint8_t val);

void outl (unsigned int value, unsigned short int port);
void insl (unsigned short int port, void *addr, unsigned long int count);

void io_wait (void);

#endif