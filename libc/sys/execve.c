#include <unistd.h>
#include <errno.h>
#include <sys/sys.h>

char **environ;

int execve(const char *filename, char *const argv[], char *const envp[])
{
	if (filename == NULL || argv == NULL) {
		int errno = EINVAL; // Invalid argument
		return -1;
	}

	int result = syscall(0x06, filename, argv, envp ? envp : environ);
	if (result == -1) {
		int errno = EIO;
	}

	return result;
}