#ifndef _SYS_TYPES_H
#define _SYS_TYPES_H

typedef int pid_t;

#define CLONE_VM     (1 << 0)
#define CLONE_FS     (1 << 1)
#define CLONE_FILES  (1 << 2)
#define CLONE_SIGHAND (1 << 3)
#define CLONE_THREAD (1 << 4)

#endif /* _SYS_TYPES_H */
