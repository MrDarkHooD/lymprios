#include <sys/sys.h>
#include <sys/types.h>
#include <stdarg.h>
#include <stdlib.h>

// Define a syscall function
long syscall(long number, ...) {
	// Declare a pointer to the arguments
	va_list args;
	va_start(args, number);

	// Get the syscall number and arguments
	long arg1 = va_arg(args, long);
	long arg2 = va_arg(args, long);
	long arg3 = va_arg(args, long);
	long arg4 = va_arg(args, long);
	long arg5 = va_arg(args, long);
	long arg6 = va_arg(args, long);

	// Call the syscall instruction
	asm volatile(
		"mov %0, %%eax\n\t"
		"mov %1, %%ebx\n\t"
		"mov %2, %%ecx\n\t"
		"mov %3, %%edx\n\t"
		"mov %4, %%esi\n\t"
		"mov %5, %%edi\n\t"
		"int $0x80\n\t"
		: /* no output */
		: "g"(number), "g"(arg1), "g"(arg2), "g"(arg3), "g"(arg4), "g"(arg5), "g"(arg6)
		: "eax", "ebx", "ecx", "edx", "esi", "edi"
	);

	va_end(args);

	// Get the result from eax
	long result;
	asm volatile("mov %%eax, %0" : "=r"(result));

	return result;
}