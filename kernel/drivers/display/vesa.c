#include <stdint.h>
#include <math.h>
#include <unistd.h>

#include "../../boot/multiboot.h"
#include "../../usr/share/fonts/default.h"

#define FNT_FONTHEIGHT 8
#define FNT_FONTWIDTH 8

int graphics_addr;
int termx=0;
int termy=0;

void init_graphics(int addr)
{
	graphics_addr = addr;
}

void draw_pixel(uint32_t x, uint32_t y, uint32_t rgbColor)
{
	multiboot_info_t *mbi;
	mbi = (multiboot_info_t *) graphics_addr;
	uint32_t * address = mbi->framebuffer_addr + y*mbi->framebuffer_pitch+x*(mbi->framebuffer_bpp/8);
	*address = rgbColor;
}

/* Clear the screen and initialize VIDEO, XPOS and YPOS. */
/* TODO: Fix this */
void cls()
{
	multiboot_info_t *mbi;
	mbi = (multiboot_info_t *) graphics_addr;
	
	for (uint32_t y = 0; y < mbi->framebuffer_height; ++y)
		for (uint32_t x = 0; x < mbi->framebuffer_width; ++x)
			draw_pixel(x, y, 0x000000);
}

void draw_box(uint32_t x, uint32_t y, uint32_t w, uint32_t h, uint32_t b, uint32_t c)
{
	//horizontal
	for(uint32_t i=0;i<w+b;i++) {
		for(uint32_t a=0;a<b;a++){
			draw_pixel((x+i), y+a, c);
			draw_pixel((x+i), y+h+a, c);
		}
	}
	//vertical
	for(uint32_t i=0;i<h+b;i++) {
		for(uint32_t a=0;a<b;a++){
			draw_pixel(x+a, y+i, c);
			draw_pixel((x+w)+a, y+i, c);
		}
	}
}

void draw_filled_box(uint32_t x, uint32_t y, uint32_t w, uint32_t h, uint32_t c)
{
	for(uint32_t i=0;i<w;i++)
		for(uint32_t a=0;a<h;a++)
			draw_pixel((x+i), y+a, c);
}

draw_circle(uint32_t xc, uint32_t yc, uint32_t x, uint32_t y, uint32_t c) 
{ 
    draw_pixel(xc+x, yc+y, c); 
    draw_pixel(xc-x, yc+y, c); 
    draw_pixel(xc+x, yc-y, c); 
    draw_pixel(xc-x, yc-y, c); 
    draw_pixel(xc+y, yc+x, c); 
    draw_pixel(xc-y, yc+x, c); 
    draw_pixel(xc+y, yc-x, c); 
    draw_pixel(xc-y, yc-x, c); 
} 

void circleBres(int xc, int yc, int r, uint32_t c) 
{ 
    int x = 0, y = r; 
    int d = 3 - 2 * r; 
    draw_circle(xc, yc, x, y, c); 
    while (y >= x) 
    { 
        // for each pixel we will 
        // draw all eight pixels 
          
        x++; 
  
        // check for decision parameter 
        // and correspondingly  
        // update d, x, y 
        if (d > 0) 
        { 
            y--;  
            d = d + 4 * (x - y) + 10; 
        } 
        else
            d = d + 4 * x + 6; 
        draw_circle(xc, yc, x, y, c); 
		sleep(2);
    } 
}

void draw_line(int x0, int y0, int x1, int y1, uint32_t c) {
 
  int dx = abs(x1-x0), sx = x0<x1 ? 1 : -1;
  int dy = abs(y1-y0), sy = y0<y1 ? 1 : -1; 
  int err = (dx>dy ? dx : -dy)/2, e2;
 
  for(;;){
    draw_pixel(x0,y0,c);
    if (x0==x1 && y0==y1) break;
    e2 = err;
    if (e2 >-dx) { err -= dy; x0 += sx; }
    if (e2 < dy) { err += dx; y0 += sy; }
  }
}


void draw_char(uint32_t xx, uint32_t yy, uint32_t text, uint32_t color)
{
	uint8_t * fnt = get_font_array();

	for(uint32_t y = 0; y < FNT_FONTHEIGHT; y++) {
		for(uint32_t x = 0; x < FNT_FONTWIDTH; x++) {
			if(fnt[text * FNT_FONTHEIGHT + y] >> (7 - x) & 1) {
				draw_pixel(xx+x, yy+y, color);
			}
		}
	}
}

void draw_newline(void)
{
	termx=0;
	termy+=10;
}

void draw_string(uint32_t x, uint32_t y, char *text, uint32_t color)
{
	while (*text) {
		int val = (int) *text;
		if(val == 10)
			draw_newline();
		else {
			draw_char(termx, termy, val, color);
			termx+=10;
		}
		*text++;
	}
}

