#ifndef VESA_H
#define VESA_H

void init_graphics(int addr);
void draw_box(uint32_t x, uint32_t y, uint32_t w, uint32_t h, uint32_t b, uint32_t c);
void draw_filled_box(uint32_t x, uint32_t y, uint32_t w, uint32_t h, uint32_t c);
void draw_char(uint32_t xx, uint32_t yy, uint32_t text, uint32_t color);
void draw_string(uint32_t x, uint32_t y, char *text, uint32_t color);
void cls();
draw_newline(void);

#endif