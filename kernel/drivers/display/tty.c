#include <stdarg.h>
#include <stdint.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/io.h>
#include "vga.h"
#include "tty.h"

size_t terminal_row;
size_t terminal_column;
uint8_t terminal_color;
uint16_t* terminal_buffer;

void enable_cursor(uint8_t cursor_start, uint8_t cursor_end)
{
	outb(0x3D4, 0x0A);
	outb(0x3D5, (inb(0x3D5) & 0xC0) | cursor_start);

	outb(0x3D4, 0x0B);
	outb(0x3D5, (inb(0x3D5) & 0xE0) | cursor_end);
}

void disable_cursor()
{
	outb(0x3D4, 0x0A);
	outb(0x3D5, 0x20);
}

void update_cursor(int x, int y)
{
	uint16_t pos = y * VGA_WIDTH + x;

	outb(0x3D4, 0x0F);
	outb(0x3D5, (uint8_t) (pos & 0xFF));
	outb(0x3D4, 0x0E);
	outb(0x3D5, (uint8_t) ((pos >> 8) & 0xFF));
}

uint8_t vga_entry_color(enum vga_color fg, enum vga_color bg) 
{
	return fg | bg << 4;
}

uint16_t vga_entry(unsigned char uc, uint8_t color) 
{
	return (uint16_t) uc | (uint16_t) color << 8;
}

void terminal_initialize(void) 
{
	terminal_row = 0;
	terminal_column = 0;
	terminal_color = vga_entry_color(VGA_COLOR_LIGHT_GREY, VGA_COLOR_BLACK);
	terminal_buffer = (uint16_t*) 0xB8000;
	for (size_t y = 0; y < VGA_HEIGHT; y++) {
		for (size_t x = 0; x < VGA_WIDTH; x++) {
			const size_t index = y * VGA_WIDTH + x;
			terminal_buffer[index] = vga_entry(' ', terminal_color);
		}
	}
}

void terminal_setcolor(uint8_t color) 
{
	terminal_color = color;
}

void terminal_putentryat(char c, uint8_t color, size_t x, size_t y) 
{
	const size_t index = y * VGA_WIDTH + x;
	terminal_buffer[index] = vga_entry(c, color);
}

void handle_newline()
{
	if(terminal_row == VGA_HEIGHT - 1) {
		// Scroll up by copying each row to the row above it
		for(size_t y = 1; y < VGA_HEIGHT; y++) {
			for(size_t x = 0; x < VGA_WIDTH; x++) {
				size_t src_index = y * VGA_WIDTH + x;
				size_t dest_index = (y - 1) * VGA_WIDTH + x;
				terminal_buffer[dest_index] = terminal_buffer[src_index];
			}
		}
		// Clear the last row
		size_t last_row_index = (VGA_HEIGHT - 1) * VGA_WIDTH;
		for(size_t x = 0; x < VGA_WIDTH; x++) {
			terminal_buffer[last_row_index + x] = vga_entry(' ', terminal_color);
		}
	} else {
		terminal_row++;
	}
	terminal_column = 0;
}

void handle_backspace() {
	if(terminal_column > 0) {
		terminal_column--;
		terminal_putentryat('\0', terminal_color, terminal_column, terminal_row);
	}
}

void handle_character(char c) {
	switch (c) {
		case '\n':
			handle_newline();
			break;

		case '\b':
			handle_backspace();
			break;

		default:
			write_char((uint32_t)c);
			break;
	}
}

void write_char(char codepoint) {

	if (codepoint <= 0x7F) { // ASCII range
		terminal_putentryat((char)codepoint, terminal_color, terminal_column, terminal_row);
	} else if (codepoint <= 0x7FF) { // Two-byte UTF-8 character
		terminal_putentryat((char)(0xC0 | (codepoint >> 6)), terminal_color, terminal_column, terminal_row);
		terminal_putentryat((char)(0x80 | (codepoint & 0x3F)), terminal_color, ++terminal_column, terminal_row);
	} else if (codepoint <= 0xFFFF) { // Three-byte UTF-8 character
		terminal_putentryat((char)(0xE0 | (codepoint >> 12)), terminal_color, terminal_column, terminal_row);
		terminal_putentryat((char)(0x80 | ((codepoint >> 6) & 0x3F)), terminal_color, ++terminal_column, terminal_row);
		terminal_putentryat((char)(0x80 | (codepoint & 0x3F)), terminal_color, ++terminal_column, terminal_row);
	} else if (codepoint <= 0x10FFFF) { // Four-byte UTF-8 character
		terminal_putentryat((char)(0xF0 | (codepoint >> 18)), terminal_color, terminal_column, terminal_row);
		terminal_putentryat((char)(0x80 | ((codepoint >> 12) & 0x3F)), terminal_color, ++terminal_column, terminal_row);
		terminal_putentryat((char)(0x80 | ((codepoint >> 6) & 0x3F)), terminal_color, ++terminal_column, terminal_row);
		terminal_putentryat((char)(0x80 | (codepoint & 0x3F)), terminal_color, ++terminal_column, terminal_row);
	}

	// Increment column position
	if (++terminal_column == VGA_WIDTH) {
		terminal_column = 0;
		if (++terminal_row == VGA_HEIGHT)
			terminal_row = 0;
	}
}

void terminal_write(const char* format, ...) {
	va_list args;
	va_start(args, format);

	char buffer[256];
	vsnprintf(buffer, sizeof(buffer), format, args);

	// Output the formatted string
	for (const char* p = buffer; *p != '\0'; ++p) {
		handle_character(*p);
	}

	va_end(args);
}

void terminal_error(const char* data) {
	uint8_t original_terminal_color = terminal_color;
	terminal_setcolor(VGA_COLOR_RED);
	terminal_write("Error: %s\n", data);
	fprintf(stderr, "Terminal error: %s\n", data);
	terminal_setcolor(original_terminal_color);
}

void terminal_writestring(const char* data) 
{
	terminal_write(data, strlen(data));
}