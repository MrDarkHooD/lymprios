#include <stdio.h>
#include <sys/io.h>
#include "cmos.h"

void get_floppy_drive_types(uint8_t *master_type, uint8_t *slave_type) {
	uint8_t drive_type = cmos_read(CMOS_FLOPPY_DRIVE_TYPE_REGISTER);
	
	/*
	 * Bits 0 to 3 = slave floppy type
	 * bits 4 to 7 = master floppy type 
	 */
	
	*master_type = (drive_type & 0xF0) >> 4;
	*slave_type = drive_type & 0x0F;
}

uint8_t get_floppy_drive_count() {
	uint8_t drive_count = cmos_read(CMOS_FLOPPY_DRIVE_COUNT_REGISTER);
	return drive_count;
}

uint8_t get_floppy_drive_string(uint8_t type) {
	switch (type) {
		case FLOPPY_DRIVE_TYPE_NONE:
			return "No floppy drive\n";
			break;
		case FLOPPY_DRIVE_TYPE_360K_525INCH:
			return "Floppy drive: 5.25\" 360kb";
			break;
		case FLOPPY_DRIVE_TYPE_12M_525INCH:
			return "Floppy drive: 5.25\" 1.2MB";
			break;
		case FLOPPY_DRIVE_TYPE_720K_35INCH:
			return "Floppy drive: 3.5\" 720kb";
			break;
		case FLOPPY_DRIVE_TYPE_144M_35INCH:
			return "Floppy drive: 3.5\" 1.44MB";
			break;
		case FLOPPY_DRIVE_TYPE_288M_35INCH:
			return "Floppy drive: 3.5\" 2.88MB";
			break;
		default:
			return "Invalid floppy drive type.\n";
			break;
	}
	return -1;
}
