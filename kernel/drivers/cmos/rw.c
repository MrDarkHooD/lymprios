#include <stddef.h>
#include <stdio.h>
#include <sys/io.h>
#include "cmos.h"

int get_cmos_update_in_progress_flag() {
	outb(CMOS_ADDRESS_PORT, 0x0A);
	return (inb(CMOS_DATA_PORT) & 0x80);
}

int cmos_read(int addr) {
	while(get_cmos_update_in_progress_flag()); // Just wait..
	outb(CMOS_ADDRESS_PORT, 0x8A); // Disable NMI
	outb(CMOS_ADDRESS_PORT, addr);
	io_wait();
	return inb(CMOS_DATA_PORT);
}

char cmos_bcd_to_binary(int ivalue) {
	/*
	 * Binary Coded Decimal to normal binary value
	 * This is not very common in modern computing,
	 * so it is stored only as CMOS function due
	 * limited need.
	*/

	return ( (ivalue & 0xF0) >> 1) + ( (ivalue & 0xF0) >> 3) + (ivalue & 0xf);
}

