#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <time.h>
#include <sys/io.h>

#include "cmos.h"

// TODO: change this from fixed to working
#define CURRENT_YEAR 2024

const char* const days[] = {
	"Saturday",
	"Sunday",
	"Monday",
	"Tuesday",
	"Wednesday",
	"Thursday",
	"Friday"
};

char get_rtc_century(void) {
	/* RTC_CENTURY addr "cheatsheet"
	 * 
	 * Set port by ACPI table parsing code if possibe
	 * 0x32 Most common BIOS implementation
	 * 0x37 BIOS implementation
	 * 0x69 Legacy systems
	 * 0x6D Legacy systems
	 * 0x74 Legacy systems, proprietary systems
	 * 0x76 Mainstream systems
	 */
	unsigned char registerB = cmos_read(RTC_B_REGISTER);
	unsigned char century	 = cmos_read(0x32);
	if (!(registerB & 0x04)) {
		century = cmos_bcd_to_binary(century);
	}

	return century;
}

int get_rtc_2digit_year(void) {
	unsigned char registerB = cmos_read(RTC_B_REGISTER);
	unsigned int year = cmos_read(RTC_YEAR);

	if (!(registerB & 0x04)) {
		year = cmos_bcd_to_binary(year);
	}

	return year;
}

int get_rtc_year(void) {
	unsigned char registerB = cmos_read(RTC_B_REGISTER);
	unsigned int year = cmos_read(RTC_YEAR);

	if (!(registerB & 0x04)) {
		year = cmos_bcd_to_binary(year);
	}

	if(RTC_CENTURY != 0) {
		unsigned char century = get_rtc_century();
		year += century * 100;
	} else {
		year += (CURRENT_YEAR / 100) * 100;
		if(year < CURRENT_YEAR) year += 100;
	}
	
	return year;
}

int get_rtc_month(void) {
	unsigned char registerB = cmos_read(RTC_B_REGISTER);
	unsigned char month = cmos_read(RTC_MONTH);
	
	if (!(registerB & 0x04)) {
		month = cmos_bcd_to_binary(month);
	}

	return month;
}

char get_rtc_day(void) {
	unsigned char registerB = cmos_read(RTC_B_REGISTER);
	unsigned char day = cmos_read(RTC_DAY);

	if (!(registerB & 0x04)) {
		day = cmos_bcd_to_binary(day);
	}

	return day;
}

int get_rtc_isdst(void) {
	unsigned char registerB = cmos_read(RTC_B_REGISTER);
	return (!(registerB & 0x01));
}

int get_rtc_hour(void) {
	unsigned char registerB = cmos_read(RTC_B_REGISTER);
	unsigned char hour			= cmos_read(RTC_HOUR);
	/*
	 * 0x09 (RTC_HOUR) register has BCD value 00-99 or Hex value of 00-63
	 * BCD/Hex selection depends on Bit 2 of register B (0Bh)
	 * 0. (0x00) Daylight Savings Enable - 1 enables
	 * 1. (0x02) 24/12 hour selection - 1 enables 24 hour mode
	 * 2. (0x04) Data Mode - 0: BCD, 1: Binary (Hex)
	 */

	if (!(registerB & 0x04)) {
		/*
		 * This differs bit from another values as lest significant byte is am/pm indicator
		 * for 12 hour clock (sometimes)
		 */
		hour = ( (hour & 0x0F) + (((hour & 0x70) / 16) * 10) ) | (hour & 0x80);
	}

	/* Convert 12 hour clock to 24 hour clock if necessary */
	if (!(registerB & 0x02) && (hour & 0x80)) {
		hour = ((hour & 0x7F) + 12) % 24;
	}
	
	return hour;
}

int get_rtc_minute(void) {
	unsigned char registerB = cmos_read(RTC_B_REGISTER);
	unsigned char minute = cmos_read(RTC_MINUTE);

	if (!(registerB & 0x04)) {
		minute = cmos_bcd_to_binary(minute);
	}

	return minute;
}

int get_rtc_second(void) {
	unsigned char registerB = cmos_read(RTC_B_REGISTER);
	unsigned char second = cmos_read(RTC_SECOND);

	if (!(registerB & 0x04)) {
		second = cmos_bcd_to_binary(second);
	}

	return (int)second;
}

int get_rtc_weekday(void) {
	unsigned char registerB = cmos_read(RTC_B_REGISTER);
	unsigned char weekday = cmos_read(RTC_DAY_OF_WEEK);

	if (!(registerB & 0x04)) {
		weekday = cmos_bcd_to_binary(weekday);
	}

	return (int)weekday;
}
