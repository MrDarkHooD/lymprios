#ifndef CMOS_H
#define CMOS_H

// CMOS port addresses
#define CMOS_ADDRESS_PORT 0x70
#define CMOS_DATA_PORT    0x71

#define RTC_B_REGISTER 0x0B

// CMOS register addresses to choose output data
#define CMOS_FLOPPY_DRIVE_TYPE_REGISTER 0x10
#define CMOS_FLOPPY_DRIVE_COUNT_REGISTER 0x14

#define RTC_SECOND 0x00
#define RTC_MINUTE 0x02
#define RTC_HOUR 0x04
#define RTC_DAY 0x07
#define RTC_MONTH 0x08
#define RTC_YEAR 0x09
#define RTC_CENTURY 0x32
#define RTC_DAY_OF_WEEK 0x06

// Floppy drive types
#define FLOPPY_DRIVE_TYPE_NONE 0x00
#define FLOPPY_DRIVE_TYPE_360K_525INCH 0x01
#define FLOPPY_DRIVE_TYPE_12M_525INCH 0x02
#define FLOPPY_DRIVE_TYPE_720K_35INCH 0x03
#define FLOPPY_DRIVE_TYPE_144M_35INCH 0x04
#define FLOPPY_DRIVE_TYPE_288M_35INCH 0x05

int cmos_read(int addr);
int get_cmos_update_in_progress_flag();
char cmos_bcd_to_binary(int ivalue);

char get_rtc_century(void);
int get_rtc_2digit_year(void);
int get_rtc_year(void);
int get_rtc_month(void);
char get_rtc_day(void);
int get_rtc_isdst(void);
int get_rtc_hour(void);
int get_rtc_minute(void);
int get_rtc_second(void);
int get_rtc_weekday(void);

void get_floppy_drive_types(uint8_t *master_type, uint8_t *slave_type);
uint8_t get_floppy_drive_count();

#endif
