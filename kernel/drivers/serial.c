#include <stdio.h>
#include <sys/io.h>

#include "serial.h"

//Intialize
int init_serial() {
	
	// COM1-8 ports are standard, anything above is in hands of god
	uint16_t ports[] = {PORT_COM1, PORT_COM2, PORT_COM3, PORT_COM4};
	int num_ports = sizeof(ports) / sizeof(ports[0]);
	
	for (int i = 0; i < num_ports; ++i) {
		uint16_t port = ports[i];
		
		outb(port + 1, 0x00);    // Disable all interrupts
		outb(port + 3, 0x80);    // Enable DLAB (set baud rate divisor)
		outb(port + 0, 0x03);    // Set divisor to 3 (lo byte) 38400 baud
		outb(port + 1, 0x00);    //                  (hi byte)
		outb(port + 3, 0x03);    // 8 bits, no parity, one stop bit
		outb(port + 2, 0xC7);    // Enable FIFO, clear them, with 14-byte threshold
		outb(port + 4, 0x0B);    // IRQs enabled, RTS/DSR set

		outb(port + 4, 0x1E);    // Set in loopback mode, test the serial chip
		outb(port + 0, 0xAE);    // Test serial chip (send byte 0xAE and check if serial
		
		// Check if serial is faulty (i.e: not same byte as sent)
		if(inb(port + 0) != 0xAE) {
			return 1;
		}
		
		// If serial is not faulty set it in normal operation mode
		// (not-loopback with IRQs enabled and OUT#1 and OUT#2 bits enabled)
		outb(port + 4, 0x0F);

		
		if (!(inb(port + 5) & 0x20)) {
			fprintf(stderr, "Failed to initialize serial port COM%d\n", (i+1));
			return -1;
		}
		
		char buffer[255];

		snprintf(buffer, "Serial port COM%d initialized\n", (i+1));

		// Output the formatted string
		for (const char* p = buffer; *p != '\0'; ++p)
			outb(port, *p);
		
	}
	return 0;
}

// Receive
int serial_received(int port) {
	return inb(port + 5) & 1;
}

char read_serial(int port) {
	while (serial_received(port) == 0);
	return inb(port);
}

// Send
int is_transmit_empty(int port) {
	return inb(port + 5) & 0x20;
}

void write_serial(int port, char a) {
	while (is_transmit_empty(port) == 0);
	outb(port, a);
}