//https://wiki.osdev.org/IDE
#include "ata.h"
#include "../../etc/timer.h"
#include "../display/vesa.h"
#include <stdio.h>

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

#include <stdlib.h>
#include <string.h>

// Initialize the ide_devices and channels arrays
struct IDEChannelRegisters   channels[2];
struct IDEInitializedDevices ide_devices[4];

CommandSet command_set_descriptions[] = {
	{ 1 << 0,  "ATA Supported" },
	{ 1 << 1,  "ATAPI Supported" },
	{ 1 << 2,  "SMART Supported" },
	{ 1 << 3,  "Security Mode Supported" },
	{ 1 << 4,  "Power Management Supported" },
	{ 1 << 5,  "Write Cache Supported" },
	{ 1 << 6,  "Look-Ahead Supported" },
	{ 1 << 7,  "Release Interrupt Supported" },
	{ 1 << 8,  "Service Interrupt Supported" },
	{ 1 << 9,  "Device Reset Supported" },
	{ 1 << 10, "Host Protected Area Supported" },
	{ 1 << 11, "Power-Up in Standby Supported" },
	{ 1 << 12, "Removable Media Status Supported" },
	{ 1 << 13, "Advanced Power Management Supported" },
	{ 1 << 14, "CompactFlash Association Advanced Timing Supported" },
	{ 1 << 15, "MicroDrive Expanded Register Supported" },
	{ 1 << 16, "Read DMA Queued Supported" },
	{ 1 << 17, "Download Microcode Supported" },
	{ 1 << 18, "SET MAX security extension Supported" },
	{ 1 << 19, "Automatic Acoustic Management Supported" },
	{ 1 << 20, "48-bit Address Supported" },
	{ 1 << 21, "Device Configuration Overlay Supported" },
	{ 1 << 22, "Mandatory FLUSH CACHE Supported" },
	{ 1 << 23, "Optional FLUSH CACHE Supported" },
	{ 1 << 24, "Device-initiated interface power state transitions Supported" },
	{ 1 << 25, "LBA48 Supported" },
	{ 1 << 26, "Streaming Supported" },
	{ 1 << 27, "Media Card Pass Through Supported" },
	{ 1 << 28, "Media Serial Number Supported" },
	{ 1 << 29, "SMART Self-test Supported" },
	{ 1 << 30, "SMART Error Logging Supported" },
};

Capability capability_descriptions[] = {
    { 0x0001, "DMA Supported" },
    { 0x0002, "LBA Supported" },
    { 0x0004, "IORDY Supported" },
    { 0x0008, "IORDY Disable" },
    { 0x0010, "Standby Timer Supported" },
};

unsigned char ide_buf[2048] = {0};

unsigned char ide_read(unsigned char channel, unsigned char reg) {
   unsigned char result=0;
   if (reg > 0x07 && reg < 0x0C)
      ide_write(channel, ATA_REG_CONTROL, 0x80 | channels[channel].nIEN);
   if (reg < 0x08)
      result = inb(channels[channel].base + reg - 0x00);
   else if (reg < 0x0C)
      result = inb(channels[channel].base  + reg - 0x06);
   else if (reg < 0x0E)
      result = inb(channels[channel].ctrl  + reg - 0x0A);
   else if (reg < 0x16)
      result = inb(channels[channel].bmide + reg - 0x0E);
   if (reg > 0x07 && reg < 0x0C)
      ide_write(channel, ATA_REG_CONTROL, channels[channel].nIEN);
   return result;
}

void ide_write(unsigned char channel, unsigned char reg, unsigned char data) {
   if (reg > 0x07 && reg < 0x0C)
      ide_write(channel, ATA_REG_CONTROL, 0x80 | channels[channel].nIEN);
   if (reg < 0x08)
      outb(channels[channel].base  + reg - 0x00, data);
   else if (reg < 0x0C)
      outb(channels[channel].base  + reg - 0x06, data);
   else if (reg < 0x0E)
      outb(channels[channel].ctrl  + reg - 0x0A, data);
   else if (reg < 0x16)
      outb(channels[channel].bmide + reg - 0x0E, data);
   if (reg > 0x07 && reg < 0x0C)
      ide_write(channel, ATA_REG_CONTROL, channels[channel].nIEN);
}

void ide_read_buffer(unsigned char channel, unsigned char reg, unsigned int buffer,
                     unsigned int quads) {

	// Check if the register is in the control range and set the control register accordingly
	if (reg > 0x07 && reg < 0x0C) {
		ide_write(channel, ATA_REG_CONTROL, 0x80 | channels[channel].nIEN);
	}

	unsigned int port;
	if (reg < 0x08) {
		port = (channels[channel].base + reg - 0x00);
	} else if (reg < 0x0C) {
		port = (channels[channel].base + reg - 0x06);
	} else if (reg < 0x0E) {
		port = (channels[channel].ctrl + reg - 0x0A);
	} else if (reg < 0x16) {
		port = (channels[channel].bmide + reg - 0x0E);
	} else {
		errno = EINVAL;
		fprintf(stderr, "Invalid register");
		return;
	}

	// Read the data from the port into the buffer
	for (unsigned int i = 0; i < quads; i++) {
		((unsigned int*)buffer)[i] = inl(port);
	}

	// Reset the control register if it was modified
	if (reg > 0x07 && reg < 0x0C) {
		ide_write(channel, ATA_REG_CONTROL, channels[channel].nIEN);
	}
}

int ide_initialize(unsigned int BAR0, unsigned int BAR1, unsigned int BAR2,
									 unsigned int BAR3, unsigned int BAR4) {
	
	if (!timer_installed) {
		errno = ENODEV;
		perror("Timer not installed");
		return -1;
	}
 
	int channel, drive, k, count = 0;
 
	// 1- Detect I/O Ports which interface IDE Controller:
	channels[ATA_PRIMARY  ].base  = (BAR0 & 0xFFFFFFFC) + 0x1F0 * (!BAR0);
	channels[ATA_PRIMARY  ].ctrl  = (BAR1 & 0xFFFFFFFC) + 0x3F6 * (!BAR1);
	channels[ATA_SECONDARY].base  = (BAR2 & 0xFFFFFFFC) + 0x170 * (!BAR2);
	channels[ATA_SECONDARY].ctrl  = (BAR3 & 0xFFFFFFFC) + 0x376 * (!BAR3);
	channels[ATA_PRIMARY  ].bmide = (BAR4 & 0xFFFFFFFC) + 0; // Bus Master IDE
	channels[ATA_SECONDARY].bmide = (BAR4 & 0xFFFFFFFC) + 8; // Bus Master IDE
	
	ide_write(ATA_PRIMARY  , ATA_REG_CONTROL, 2);
	ide_write(ATA_SECONDARY, ATA_REG_CONTROL, 2);

	// 3- Detect ATA-ATAPI Devices:
	for (channel = 0; channel < 2; channel++) {
		for (drive = 0; drive < 2; drive++) {

			bool is_ata = true;
			unsigned char type = IDE_ATA;
			unsigned char status;
	
			ide_devices[count].Reserved = 0; // Assuming that no drive here.

			// (I) Select Drive:
			ide_write(channel, ATA_REG_HDDEVSEL, 0xA0 | (drive << 4)); // Select Drive.
			timer_wait(1); // Wait 1ms for drive select to work.

			// (II) Send ATA Identify Command:
			ide_write(channel, ATA_REG_COMMAND, ATA_CMD_IDENTIFY);
			timer_wait(1);

			// (III) Polling:
			if (ide_read(channel, ATA_REG_STATUS) == 0) continue; // If Status = 0, No Device.

			while(1) {
				status = ide_read(channel, ATA_REG_STATUS);
				if (status & ATA_SR_ERR) {
					// Device is not ATA.
					is_ata = false;
					break;
				}

				if (!(status & ATA_SR_BSY) && (status & ATA_SR_DRQ)) break; // Everything is right.
			}

			// Probe for ATAPI Devices:
			if (!is_ata) {
				unsigned char cl = ide_read(channel, ATA_REG_LBA1);
				unsigned char ch = ide_read(channel, ATA_REG_LBA2);

				if ((cl == 0x14 && ch == 0xEB) ||
						(cl == 0x69 && ch == 0x96)) {
					type = IDE_ATAPI;
				} else {
					fprintf(stderr, "Unknown device type detected at channel %d, drive %d\n", channel, drive);
					continue; // Unknown Type (may not be a device).
				}

				ide_write(channel, ATA_REG_COMMAND, ATA_CMD_IDENTIFY_PACKET);
				timer_wait(1);
			}

			// (V) Read Identification Space of the Device:
			ide_read_buffer(channel, ATA_REG_DATA, (unsigned int) ide_buf, 128);

			// (VI) Read Device Parameters:
			ide_devices[count].Reserved     = 1;
			ide_devices[count].Type         = type;
			ide_devices[count].Channel      = channel;
			ide_devices[count].Drive        = drive;
			ide_devices[count].Signature    = *((unsigned short *)(ide_buf + ATA_IDENT_DEVICETYPE));
			ide_devices[count].Capabilities = *((unsigned short *)(ide_buf + ATA_IDENT_CAPABILITIES));
			ide_devices[count].CommandSets  = *((unsigned int *)(ide_buf + ATA_IDENT_COMMANDSETS));

			// (VII) Get Size:
			if (ide_devices[count].CommandSets & (1 << 26)) {
				// Device uses 48-Bit Addressing:
				ide_devices[count].Size   = *((unsigned int *)(ide_buf + ATA_IDENT_MAX_LBA_EXT));
			} else {
				// Device uses CHS or 28-bit Addressing:
				ide_devices[count].Size   = *((unsigned int *)(ide_buf + ATA_IDENT_MAX_LBA));
			}

			// (VIII) String indicates model of device (like Western Digital HDD and SONY DVD-RW...):
			for(k = 0; k < 40; k += 2) {
				ide_devices[count].Model[k] = ide_buf[ATA_IDENT_MODEL + k + 1];
				ide_devices[count].Model[k + 1] = ide_buf[ATA_IDENT_MODEL + k];
			}
			ide_devices[count].Model[40] = 0; // Terminate String.

			// (IX) String indicates serial number
			for (int k = 0; k < 20; k++) {
				ide_devices[count].Serial[k] = ide_buf[ATA_IDENT_SERIAL + k + 1];
				ide_devices[count].Serial[k + 1] = ide_buf[ATA_IDENT_SERIAL + k];
			}
			ide_devices[count].Serial[20] = 0;

			count++;
		}
	}
	return 0;
}

void output_installed_ata_drives() {

	fprintf (stdout, "Installed ATA* drives:\n");
	for (int i = 0; i < 4; i++) {
		if (ide_devices[i].Reserved != 1) {
			continue;
		}

		// Format and print size
		char size_str[20];
		format_size(size_str, ide_devices[i].Size);

		fprintf(stdout, "  %s Drive - [%s %s]:\n",
						(const char *[]){"ATA", "ATAPI"}[ide_devices[i].Type],
						(const char *[]){"Primary", "Secondary"}[ide_devices[i].Channel],
						(const char *[]){"Master", "Slave"}[ide_devices[i].Drive]
		);
		fprintf (stdout, "    Index: %d\n", i);
		fprintf (stdout, "    Model: %s\n", ide_devices[i].Model);
		fprintf (stdout, "    Size: %u\n", ide_devices[i].Size);
		fprintf (stdout, "    Signature %d\n", ide_devices[i].Signature);
		fprintf (stdout, "    Serial %s\n", ide_devices[i].Serial);

		fprintf (stdout, "    Capabilities:\n");
		ata_output_capabilities_hr(ide_devices[i].Capabilities);
		
		fprintf (stdout, "    Command sets:\n");
		ata_output_command_hr(ide_devices[i].CommandSets);

	}
}

unsigned char ide_print_error(unsigned int drive, unsigned char err) {
   if (err == 0)
      return err;
 
   printf("IDE:");
   if (err == 1) {printf("- Device Fault\n     "); err = 19;}
   else if (err == 2) {
      unsigned char st = ide_read(ide_devices[drive].Channel, ATA_REG_ERROR);
      if (st & ATA_ER_AMNF)   {printf("- No Address Mark Found\n     ");   err = 7;}
      if (st & ATA_ER_TK0NF)   {printf("- No Media or Media Error\n     ");   err = 3;}
      if (st & ATA_ER_ABRT)   {printf("- Command Aborted\n     ");      err = 20;}
      if (st & ATA_ER_MCR)   {printf("- No Media or Media Error\n     ");   err = 3;}
      if (st & ATA_ER_IDNF)   {printf("- ID mark not Found\n     ");      err = 21;}
      if (st & ATA_ER_MC)   {printf("- No Media or Media Error\n     ");   err = 3;}
      if (st & ATA_ER_UNC)   {printf("- Uncorrectable Data Error\n     ");   err = 22;}
      if (st & ATA_ER_BBK)   {printf("- Bad Sectors\n     ");       err = 13;}
   } else  if (err == 3)  {printf("- Reads Nothing\n     "); err = 23;}
     else  if (err == 4)  {printf("- Write Protected\n     "); err = 8;}
   printf("- [%s %s] %s\n",
      (const char *[]){"Primary", "Secondary"}[ide_devices[drive].Channel], // Use the channel as an index into the array
      (const char *[]){"Master", "Slave"}[ide_devices[drive].Drive], // Same as above, using the drive
      ide_devices[drive].Model);
 
   return err;
}