#ifndef BMP_H
#define BMP_H

void display_bmp(uint8_t *image_data, uint32_t start_x, uint32_t start_y, int scale_width, int scale_height);

#endif