#include <stdint.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>

#include "../display/vesa.h"

#pragma pack(push, 1)
typedef struct {
	uint16_t bfType;
	uint32_t bfSize;
	uint16_t bfReserved1;
	uint16_t bfReserved2;
	uint32_t bfOffBits;
} BMPFileHeader;

typedef struct {
	uint32_t biSize;
	int32_t  biWidth;
	int32_t  biHeight;
	uint16_t biPlanes;
	uint16_t biBitCount;
	uint32_t biCompression;
	uint32_t biSizeImage;
	int32_t  biXPelsPerMeter;
	int32_t  biYPelsPerMeter;
	uint32_t biClrUsed;
	uint32_t biClrImportant;
} BMPInfoHeader;
#pragma pack(pop)

void display_bmp(uint8_t *image_data, uint32_t start_x, uint32_t start_y,
								 int scale_width, int scale_height) {
	BMPFileHeader *fileHeader = (BMPFileHeader *)image_data;
	BMPInfoHeader *infoHeader = (BMPInfoHeader *)(image_data + sizeof(BMPFileHeader));

	if (fileHeader->bfType != 0x4D42) { // Check if it's a BMP file
		errno = EFTYPE;
		return;
	}

	int width = infoHeader->biWidth;
	int height = infoHeader->biHeight;
	uint8_t *pixelData = (uint8_t *)(image_data + fileHeader->bfOffBits);

	for (int y = 0; y < height; y++) {
		for (int x = 0; x < width; x++) {
			
			// Calculate pixel offset (BMP stores pixels bottom-to-top)
			int pixelIndex = ((height - 1 - y) * width + x) * (infoHeader->biBitCount / 8);
			uint8_t blue = pixelData[pixelIndex];
			uint8_t green = pixelData[pixelIndex + 1];
			uint8_t red = pixelData[pixelIndex + 2];
			uint8_t alpha = pixelData[pixelIndex + 3]; // Transparency value (if available)

			// Check if pixel is fully transparent
			if (alpha == 0x00) continue;

			// Quite basic scaling
			ldiv_t rel_width, rel_height;
			rel_width = ldiv(width, scale_width);
			rel_height = ldiv(height, scale_height);

			if (x % rel_width.quot == 0 || y % rel_height.quot == 0) continue;
			
			int scaled_x = start_x + x / rel_width.quot;
			int scaled_y = start_y + y / rel_height.quot;
			
			// Calculate color with alpha blending
			uint32_t background_color = 0x000000; // Get background color at (start_x + x, start_y + y)
			uint8_t alpha_inverse = 0xFF - alpha;
			uint8_t blended_red = (red * alpha + (background_color >> 16 & 0xFF) * alpha_inverse) / 255;
			uint8_t blended_green = (green * alpha + (background_color >> 8 & 0xFF) * alpha_inverse) / 255;
			uint8_t blended_blue = (blue * alpha + (background_color & 0xFF) * alpha_inverse) / 255;
			uint32_t color = ((uint32_t)blended_red << 16) | ((uint32_t)blended_green << 8) | (uint32_t)blended_blue;
			
			draw_pixel(scaled_x, scaled_y, color);
		}
	}
}
