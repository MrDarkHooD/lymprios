#ifndef KEYBOARD_H
#define KEYBOARD_H

#define KEYBOARD_ADDR 0x60
#define SCANCODE_MAX 255

void setkmap();
void clear_keyboard_buffer(void);
uint8_t get_keyboard_char(void);
void keyboard_handler(Stack *registers);
void keyboard_install(void);

static const char* const kblayoutmap_us[] = {
	[0x01] = 0x01,
	[0x02] = "1!", 
	[0x03] = "2@", 
	[0x04] = "3#",
	[0x05] = "4$", 
	[0x06] = "5%", 
	[0x07] = "6^", 
	[0x08] = "7&", 
	[0x09] = "8*", 
	[0x0A] = "9(", 
	[0x0B] = "0)", 
	[0x0C] = "-_", 
	[0x0D] = "=+", 
	[0x0E] = "\b", // backspace
	[0x0F] = "\t", // tab
	[0x10] = "qQ", 
	[0x11] = "wW", 
	[0x12] = "eE", 
	[0x13] = "rR", 
	[0x14] = "tT", 
	[0x15] = "yY", 
	[0x16] = "uU", 
	[0x17] = "iI", 
	[0x18] = "oO", 
	[0x19] = "pP", 
	[0x1A] = "[{", 
	[0x1B] = "]}", 
	[0x1C] = "\n", // enter
	[0x1E] = "aA", 
	[0x1F] = "sS", 
	[0x20] = "dD",
	[0x21] = "fF",
	[0x22] = "gG",
	[0x23] = "hH",
	[0x24] = "jJ",
	[0x25] = "kK",
	[0x26] = "lL",
	[0x27] = ";:",
	[0x28] = "'\"",
	[0x2B] = "\\|",
	[0x2C] = "zZ",
	[0x2D] = "xX",
	[0x2E] = "cC",
	[0x2F] = "vV",
	[0x30] = "bB",
	[0x31] = "nN",
	[0x32] = "mM",
	[0x33] = ",<",
	[0x34] = ".>",
	[0x35] = "/?",
	[0x39] = "  ", //Dual space as dirty fix for shift combo
};

#endif
