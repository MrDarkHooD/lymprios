#include <string.h>
#include <stdbool.h>
#include <stdio.h>
#include <stddef.h>
#include <stdint.h>
#include <sys/sys.h>
#include <sys/io.h>
#include "input.h"
#include "../../../interrupts/interrupts.h"

bool CapsLockActive        = false;
bool RightShiftPressed     = false;
bool LeftShiftPressed      = false;
bool RightControlPressed   = false;
bool LeftControlPressed    = false;
volatile char* keyboard_buffer = (char*)0x1000;

void clear_keyboard_buffer(void)
{
	*keyboard_buffer = '\0';
}

uint8_t get_keyboard_char(void)
{
	return *keyboard_buffer;
}

void send_keyboard_command(uint8_t command) {
	// Wait for the keyboard controller to be ready
	while ((inb(KEYBOARD_STATUS_ADDR) & 0x02) != 0);

	// Send the command to the keyboard controller
	outb(KEYBOARD_ADDR, command);
}

void set_indicator_led(unsigned int indicator, int state) {

	if ((indicator & LED_CAPS_LOCK) != 0 && 
			(indicator & LED_NUM_LOCK) != 0 && 
			(indicator & LED_SCROLL_LOCK) != 0) {
		fprintf(stderr, "Unknown indicator");
	}

	uint8_t leds = inb(KEYBOARD_STATUS_ADDR);
	if (state) {
		leds |= indicator;
	} else {
		leds &= ~indicator;
	}
	send_keyboard_command(CMD_SET_LEDS);
	send_keyboard_command(leds);
}

void keyboard_handler(Stack *registers)
{

	// Preserving the state of the registers (done by the interrupt framework usually)
	(void)registers; // Mark the parameter as unused to avoid compiler warnings

	uint8_t scan_code = inb(KEYBOARD_ADDR);

	switch(scan_code) {
		case 0x00:
		case 0xFF:
			fprintf(stderr, "Key detection error or internal buffer overrun");
			break;

		case 0x3A:
			CapsLockActive = !CapsLockActive;
			fprintf(stdout, "Capslock state:\n");
			fprintf(stdout, (CapsLockActive) ? "\tactive\n": "\tdisabled\n");
			break;

		case 0x2A:
			LeftShiftPressed = true;
			fprintf(stdout, "Left shift pressed:\n");
			break;

		case 0x36:
			RightShiftPressed = true;
			fprintf(stdout, "Right shift pressed:\n");
			break;

		case 0xAA:
			LeftShiftPressed = false;
			fprintf(stdout, "Left shift released:\n");
			break;

		case 0xB6:
			RightShiftPressed = false;
			fprintf(stdout, "Right shift released:\n");
			break;
			
		case 0x1D:
			LeftControlPressed = true;
			fprintf(stdout, "Left control pressed:\n");
			break;

		case 0x9D:
			LeftControlPressed = false;
			fprintf(stdout, "Left control released:\n");
			break;

		case 0xEE: // Response to "0xEE (echo)" command
			fprintf(stdout, "Keyboard response:\n");
			fprintf(stdout, *keyboard_buffer);
			fprintf(stdout, "\n");
			break;

		case 0xFA: // Command acknowledged (ACK) 
			fprintf(stdout, "Keyboard acknowledged\n");
			break;

		case 0xFC:
		case 0xFD: // Self test failed (sent after "0xFF (reset)" command or keyboard power up) 
			printf(stderr, "Keyboard self test failed");
			break;

		case 0xFE: // Resend
			fprintf(stderr, "Keyboard requests resend");
			break;
		default:

			if (scan_code < 0x00 ||
					scan_code >= 0x3A ||
					!(sizeof(kblayoutmap_us) / sizeof(kblayoutmap_us[scan_code]))) {

				break;
			}
			char* mapped = kblayoutmap_us[scan_code];

			if (mapped == NULL || mapped == '\0' || mapped == "") {
				break;
			}

			if (strlen(mapped) >= 1) {

				printf("%d", scan_code);

				bool capsAffected = false;
				// Alphabets, caps lock affects these
				if ((scan_code >= 0x10 && scan_code <= 0x19) ||
            (scan_code >= 0x1E && scan_code <= 0x26) ||
            (scan_code >= 0x2C && scan_code <= 0x32)) {
					capsAffected = true;
				}

				if ((CapsLockActive && capsAffected) ^ ( RightShiftPressed2 || LeftShiftPressed2)) {
					// Convert to uppercase if Caps Lock or Shift is active
					if(strlen(mapped) >= 4) { // UTF-8
						*keyboard_buffer = mapped+strlen(mapped)/2;
					} else {
						*keyboard_buffer = mapped[1];
					}
				} else {
					// Keep Lowercase
					if(strlen(mapped) >= 4) { // UTF-8
						*keyboard_buffer = mapped[0];
						*keyboard_buffer += mapped[1];
					} else {
						*keyboard_buffer = mapped[0];
					}
				}
			} else {
				*keyboard_buffer = mapped[0];
			}

			break;
	}
}

void keyboard_install(void)
{
	irq_install_handler(1, keyboard_handler);
}
