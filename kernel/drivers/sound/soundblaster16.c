//https://wiki.osdev.org/Sound_Blaster_16

#include <stdbool.h>
#include <stdio.h>
#include <sys/io.h>
#include <unistd.h>
#include "soundblaster16.h"

bool sound_blaster=0;

void write_DSP(int value)
{
	inb(DSP_WRITE);
	outb(DSP_WRITE, value);
}

int read_DSP(void)
{
	return inb(DSP_READ);
} 

void reset_DSP(void)
{
	outb(DSP_RESET, 1);
	usleep(3); // wait 3 microseconds
	outb(DSP_RESET, 0);
 
	if(read_DSP()==0xAA)
		sound_blaster=1; 
}
 
void sb16_init(void)
{
	reset_DSP();

	if(sound_blaster==0)
		return;

	write_DSP(DSP_CMD_VERSION);
	//sb16_version_major=read_DSP();
	//sb16_version_minor=read_DSP();
}
