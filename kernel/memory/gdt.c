#include <stdint.h>
#include <string.h>
#include <stdbool.h>
#include <stdio.h>
#include <sys/halt.h>

#include "gdt.h"

Gdt_entry gdt[NB_GDT_ENTRY];
Gdt_ptr gdt_ptr;
Tss_entry tss;

extern void gdt_flush(void);
extern void tss_flush(void);

int check_gdt_entry(Gdt_entry entry, uint32_t base, uint32_t limit, uint8_t access, uint8_t granularity) {

	if (entry.limit_low != (limit & 0xFFFF)) return 1;
	if (entry.base_low != (base & 0xFFFF)) return 2;
	if (entry.base_middle != ((base >> 16) & 0xFF)) return 3;
	if (entry.base_high != ((base >> 24) & 0xFF)) return 4;
	if (entry.granularity != granularity) return 5;
	if (entry.access != access) return 6;

	return 0;
}

static int gdt_set_entry(uint8_t num, uint32_t base, uint32_t limit, uint8_t access, uint8_t granularity, char* name)
{
	fprintf(stdout, "Setting up GDT for %s\n", name);

	gdt[num].base_low = base & 0xFFFF;
	gdt[num].base_middle = (base >> 16) & 0xFF;
	gdt[num].base_high = (base >> 24) & 0xFF;

	gdt[num].limit_low = limit & 0xFFFF;

	// Clear the existing value of granularity before setting it
	gdt[num].granularity = 0;
	gdt[num].granularity = (granularity & 0xF0) | ((limit >> 16) & 0x0F);

	gdt[num].access = access;

	Gdt_entry entry = gdt[num];

	int gdt_status = check_gdt_entry(entry, base, limit, access, granularity);
	char* status_msg;
	switch(gdt_status) {
		case 0:
			status_msg = "GDT entry fine";
			break;

		case 1:
			status_msg = "Limit_low does not match";
			break;

		case 2:
			status_msg = "Base_low does not match";
			break;

		case 3:
			status_msg = "Base_middle does not match";
			break;

		case 4:
			status_msg = "Base_high does not match";
			break;

		case 5:
			status_msg = "Granularity does not match";
			break;

		case 6:
			status_msg = "Access does not match";
			break;

		default:
			status_msg = "Unknown status code";
			break;
	}

	if (gdt_status == 0) {
		fprintf(stdout, "%d. %s in %s\n", gdt_status, status_msg, name);
	} else {

		fprintf(stderr, "GDT erro no. %d: %s in %s\n", gdt_status, status_msg, name);
		fprintf(stdout, "GDT Entry %d:\n", num);
		fprintf(stdout, "  Base Low: %x\n", gdt[num].base_low);
		fprintf(stdout, "  Base Middle: %x\n", gdt[num].base_middle);
		fprintf(stdout, "  Base High: %x\n", gdt[num].base_high);
		fprintf(stdout, "  Limit Low: %x\n", gdt[num].limit_low);
		fprintf(stdout, "  Granularity: %x\n", gdt[num].granularity);
		fprintf(stdout, "  Access: %x\n", gdt[num].access);
		
		printf("GDT erro no. %d: %s in %s\n", gdt_status, status_msg, name);
		printf("GDT Entry %d:\n", num);
		printf("  Base Low: %04x\n", gdt[num].base_low);
		printf("  Base Middle: %02x\n", gdt[num].base_middle);
		printf("  Base High: %02x\n", gdt[num].base_high);
		printf("  Limit Low: %04x\n", gdt[num].limit_low);
		printf("  Granularity: %02x\n", gdt[num].granularity);
		printf("  Access: %02x\n", gdt[num].access);
		freeze_system();
	}

	return gdt_status;
}

static void tss_install(uint8_t num, uint16_t kernel_ss, uint16_t kernel_esp)
{
	uint32_t base = (uint32_t) &tss;
	uint32_t limit = sizeof(Tss_entry) - 1;

	gdt_set_entry(num, base, limit, 0xE9, 0x00, "TSS");

	memset(&tss, 0, sizeof(Tss_entry));

	tss.ss0 = kernel_ss;
	tss.esp0 = kernel_esp;

	tss.cs = 0x0B;
	tss.ss = 0x13;
	tss.es = 0x13;
	tss.ds = 0x13;
	tss.fs = 0x13;
	tss.gs = 0x13;
}

int gdt_install(void)
{
	gdt_ptr.base = (uint32_t)&gdt;  // Set base address to the address of the GDT array
	gdt_ptr.limit = (sizeof(Gdt_entry) * NB_GDT_ENTRY) - 1;

	/* NULL descriptor */
	gdt_set_entry(0, 0x0, 0x0, 0x0, 0x0, "NULL");
	/* Kernel code segment descriptor */
	gdt_set_entry(1, 0x0, 0xFFFFFFFF, 0x9A, 0xCF, "Kernel code segment");
	/* Kernel data segment descriptor */
	gdt_set_entry(2, 0x0, 0xFFFFFFFF, 0x92, 0xCF, "Kernel data");
	/* User code segment descriptor */
	gdt_set_entry(3, 0x0, 0xFFFFFFFF, 0xFA, 0xCF, "User code");
	/* User data segment descriptor */
	gdt_set_entry(4, 0x0, 0xFFFFFFFF, 0xF2, 0xCF, "User data");
	/* Task state register (TSS) */
	tss_install(5, 0x10, 0x0);

	if (gdt_ptr.base == (uint32_t) &gdt && gdt_ptr.limit == (sizeof(Gdt_entry) * NB_GDT_ENTRY) - 1) {
		fprintf(stdout, "GDT pointer is properly set.\n");
	} else {
		fprintf(stderr, "Error: GDT pointer is not properly set.\n");
		fprintf(stderr, "gdt_ptr.base: %x\n", gdt_ptr.base);
		fprintf(stderr, "gdt_ptr.limit: %x\n", gdt_ptr.limit);
		return 1;
	}

	gdt_flush();
	tss_flush();
	return 0;
}