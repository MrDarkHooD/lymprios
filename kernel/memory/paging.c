#include <stdint.h>
#include <sys/halt.h>
#include <stdio.h>

#define PAGE_SIZE 4096
#define PAGE_TABLE_SIZE 1024
#define PAGE_DIRECTORY_SIZE 1024
#define PAGE_TABLE_ENTRIES 1024

// Define structures for page table and page directory entries
typedef struct {
	uint32_t present : 1;
	uint32_t rw : 1;
	uint32_t user : 1;
	uint32_t accessed : 1;
	uint32_t dirty : 1;
	uint32_t unused : 7;
	uint32_t frame : 20;
} __attribute__((packed)) page_table_entry_t;

typedef struct {
	uint32_t present : 1;
	uint32_t rw : 1;
	uint32_t user : 1;
	uint32_t accessed : 1;
	uint32_t unused : 7;
	uint32_t pdbr : 20; // Page directory base address
} __attribute__((packed)) page_directory_entry_t;

static page_table_entry_t page_table[PAGE_TABLE_SIZE] __attribute__((aligned(PAGE_SIZE)));
static page_directory_entry_t page_directory[PAGE_DIRECTORY_SIZE] __attribute__((aligned(PAGE_SIZE)));

void setup_page_tables() {
	// Initialize page table entries
	uint32_t physical_address;
	uint32_t cr3;
	cr3 = (uint32_t)page_directory;
	for (int i = 0; i < PAGE_TABLE_SIZE; i++) {
		physical_address = cr3 + i * PAGE_SIZE;
			
		page_table_entry_t pte;
		pte.present = 1;
		pte.rw = 1;
		pte.user = 0;
		pte.accessed = 0;
		pte.dirty = 0;
		pte.unused = 0;
		pte.frame = physical_address >> 12; // Map each page to its own frame
		page_table[i] = pte;
	}

	// Initialize page directory entries
	page_directory_entry_t pd_entry;
	pd_entry.present = 1;
	pd_entry.rw = 1;
	pd_entry.user = 0;
	pd_entry.accessed = 0;
	pd_entry.unused = 0;
	pd_entry.pdbr = (uint32_t)page_table >> 12; // Page Directory Base Register
	
	// Fill the page directory with the same entry
	for (int i = 0; i < PAGE_DIRECTORY_SIZE; i++) {
		page_directory[i] = pd_entry;
	}
	fprintf(stdout, "(uint32_t)page_table: %x\n", (uint32_t)page_table);
}

void verify_page_directory() {
	// Convert the physical address to a pointer
	uint32_t cr3;
	cr3 = (uint32_t)page_directory;
	page_directory_entry_t *pd_entry = (page_directory_entry_t *)cr3;

	// Check if the page directory entry is present and points to a valid page table
	if (pd_entry->present && pd_entry->pdbr != 0) {
		// Convert the physical address of the page table to a pointer
		page_table_entry_t *pt_entry = (page_table_entry_t *)(pd_entry->pdbr << 12);

		uint16_t notpresent_pte_count = 0;
		// Iterate through page table entries
		for (int i = 0; i < PAGE_TABLE_ENTRIES; ++i) {
			// Check if the page table entry is present and points to a valid frame
			if (pt_entry[i].present && pt_entry[i].frame != 0) {
				//fprintf(stdout, "Page Table Entry %d: Present, Frame: %x\n", i, pt_entry[i].frame);
			} else {
				notpresent_pte_count++;
			}
		}
		fprintf(stdout, "Not present pt_entries: %d\n", notpresent_pte_count);
	} else {
		fprintf(stdout, "pd_entry not present.\n");
	}
}

void enable_paging() {
	uint32_t cr0;
	uint32_t cr3;
		
	verify_page_directory();
	// Load the physical address of the page directory into CR3
	cr3 = (uint32_t)page_directory;
	fprintf(stdout, "Physical address of page directory: %x\n", cr3);
	fprintf(stdout, "Value of CR3 before loading: %x\n", cr3);
	
	asm volatile ("mov %0, %%cr3" :: "r"(cr3));
	asm volatile ("mov %%cr3, %0" : "=r"(cr3));
	fprintf(stdout, "Value of CR3 after loading: %x\n", cr3);
	
	// Set the PG (Paging) bit in CR0 to enable paging
	asm volatile ("mov %%cr0, %%eax" : "=r"(cr0));
	cr0 |= 0x80000000;
	fprintf(stdout, "Updated cr0: %x\n", cr0);
	
	//asm volatile ("mov %%eax, %%cr0" :: "r"(cr0));

}
