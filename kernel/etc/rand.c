//https://wiki.osdev.org/Random_Number_Generator
#include "rand.h"
#include <string.h>
static int random_seed=3;
int rand(int max)
{
	if(max == 0) max = 255;
	random_seed = random_seed+1 * 1103515245 +12345;
	return (unsigned int)(random_seed / 65536) % (255+1); 
}
