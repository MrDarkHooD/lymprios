#include <stdbool.h>
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>
#include <time.h>
#include <sys/sys.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <dirent.h>

#include "../../kernel/drivers/cmos/cmos.h"
#include "../drivers/input/keyboard/input.h"
#include "../interrupts/interrupts.h"
#include "../drivers/cmos/cmos.h"
#include "../drivers/display/tty.h"
#include "../drivers/media/floppy.h"
#include "shell.h"
#include "lspci.h"
#include "lscpu.h"

void shell_command_floppys() {
	uint8_t master_type, slave_type;
	get_floppy_drive_types(&master_type, &slave_type);

	terminal_write("Master: %s\n", get_floppy_drive_string(master_type));
	terminal_write("Slave: %s\n", get_floppy_drive_string(slave_type));
}

void shell_command_frk() {
	pid_t pid = fork();
	if (pid == 0) {
		fprintf(stdout, "Child started\n");
	} else if (pid > 0) {
		fprintf(stdout, "Waiting pid\n");
		int status;
		waitpid(pid, &status, 0);
	} else {
		terminal_error("Fork failed");
	}
	terminal_write("Returning from fork %d", pid);
}

void shell_command_run(char* name, char* args) {
	pid_t pid = fork();
	if (pid == -1) {
		fprintf(stdout, "Fatal fork error\n");
	}else if (pid == 0) {
		fprintf(stdout, "Child started\n");

		// Execute the program
		char *argv[] = {name, NULL};
		execve("/usr/bin/hello.o", argv, NULL);

		// If execve returns, it means an error occurred
		//perror("execve");
		terminal_error("Failed to execute");
		exit(EXIT_FAILURE);
	} else if (pid > 0) {
		int status;
		waitpid(pid, &status, 0);
		if (WIFEXITED(status)) {
			printf("Child process exited with status %d\n", WEXITSTATUS(status));
		} else {
			printf("Child process terminated abnormally\n");
		}
	} else {
		terminal_error("Fork failed");
		exit(EXIT_FAILURE);
	}
}

void shell_command_showtime() {
	int second  = get_rtc_second();
	int minute  = get_rtc_minute();
	int hour    = get_rtc_hour();
	int day     = get_rtc_day();
	int month   = get_rtc_month();
	int year    = get_rtc_year();
	int wday    = get_rtc_weekday();
	int isdst   = get_rtc_isdst();

	terminal_write("%RTC: %d.%d.%d %d:%d:%d", year, month, day, hour, minute, second);

	time_t rawtime;
	time(&rawtime);

	char unixbuff[20];
	itoa(unixbuff, 10, rawtime);

	terminal_writestring(unixbuff);
	terminal_writestring("\n");
	struct tm *local_time = localtime(&rawtime);

	char buff[255];
	strftime(buff, 255,
					 "%%d: %d\n%%H: %H\n%%I: %I\n%%m: %m\n%%M: %M\n%%p: %p\n%%S: %S\n%%w: %w\n%%x: %x\n%%X: %X\n%%y: %y\n%%Y: %Y\n%%Z: %Z\n%%%%: %%\n", local_time);
	terminal_write("Strftime: %s\n", buff);
}

void shell_command_scco(void) {
	//SCan COde
	char latest;
	for(;;) {
		char scancode = get_keyboard_scancode();
		char* character = get_keyboard_char();
		if(scancode != latest) {
			terminal_write("%s:%d:%X;", character, scancode, scancode);
			latest = scancode;
		}

		// Temp until SYSINT works
		if(character == "c") break;
	}
}

void shell_parse_input(const char * input)
{
	char command[256];
	char arguments[256];

	const char *space_position = strchr(input, ' ');
	if (space_position == NULL) {
		strcpy(command, input);
		arguments[0] = '\0';
	} else {
		size_t command_length = space_position - input;
		strncpy(command, input, command_length);
		command[command_length] = '\0';
		strcpy(arguments, space_position + 1);
	}

	if (strcmp(command, "run") == 0) {
		shell_command_run(command, arguments);
	} else if (strcmp(command, "flp") == 0) {
		shell_command_floppys();
	} else if (strcmp(command, "scco") == 0) {
		shell_command_scco();
	} else if (strcmp(command, "frk") == 0) {
		shell_command_frk();
	} else if (strcmp(command, "hdd") == 0) {
		get_hdd_drive_info();
	} else if (strcmp(command, "lspci") == 0) {
		lspci();
	} else if (strcmp(command, "lscpu") == 0) {
		lscpu();
	} else if (strcmp(command, "time") == 0) {
		shell_command_showtime();
	} else {
		printf("Unknown command: %s\n", command);
	}
}

void process_input() {
	// Process the user input from the keyboard buffer
	char keyboard_buffer[255];
	int buffer_index = 0;

	terminal_writestring("> ");

	for (;;) {
		char character = get_keyboard_char();

		if (character != '\0' && character != NULL && character != "") {
			handle_character(character);

			switch(character) {
				case '\n':
					keyboard_buffer[buffer_index] = '\0'; // Terminate the string
					shell_parse_input(keyboard_buffer);
					memset(keyboard_buffer, 0, sizeof(keyboard_buffer));
					buffer_index = 0;
					terminal_writestring("> ");
					break;

				case '\b':
					keyboard_buffer[buffer_index--] = '\0';
					break;

				default:
					if (buffer_index+1 >= sizeof(keyboard_buffer)) {
						terminal_error("Keyboard buffer full");
						break;
					}
					keyboard_buffer[buffer_index++] = character;
					break;

			}
			clear_keyboard_buffer();
		}
	}
}

char* get_current_mode() {
	uint32_t cr0;
	char* mode;
	asm volatile("mov %%cr0, %0" : "=r" (cr0));

	if (!(cr0 & (1 << 0))) {
		mode = "Real (16bit)";
	} else {
		uint32_t cr4;
		asm volatile("mov %%cr4, %0" : "=r" (cr4));
		if (cr4 & (1 << 6)) {
			mode = "Long (64bit)";
		} else {
			mode = "Protected (32bit)";
		}
	}
	
	return mode;
}

uint16_t get_current_ring(void) {
	uint16_t privilege_level;
	asm volatile (
		"mov %%cs, %0\n\t"         // Move CS register value to privilege_level
		"and $0b11, %0\n\t"        // Mask out all but the two least significant bits
		: "=r" (privilege_level)   // Output operand constraint (privilege_level)
		:                          // No input operands
		:                          // No clobbered registers
	);

  return privilege_level;
}

int shell_init(void)
{
	fprintf(stdout, "shell started\n");
	char* privilege_level = get_current_ring();
	char* processing_mode = get_current_mode();

	// Make is char as int 0 doesn't work for printing atm
	char str_pl[8];
	itoa(str_pl, 10, privilege_level);
	
	terminal_write("Welcome to shell, Ring %s, %s.\n", str_pl, processing_mode);
	clear_keyboard_buffer();

	for(;;) {
		process_input();
	}
}
