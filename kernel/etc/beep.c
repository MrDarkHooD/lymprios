// https://wiki.osdev.org/PC_Speaker

#include "beep.h"
#include <stdio.h> 
#include "timer.h"

//Play sound using built in speaker
void play_sound(uint32_t nFrequence)
{
	uint32_t Div;
	uint8_t tmp;
 
	//Set the PIT to the desired frequency
	Div = 1193180 / nFrequence;
	outb(0x43, 0xb6);
	outb(0x42, (uint8_t) (Div) );
	outb(0x42, (uint8_t) (Div >> 8));

	//And play the sound using the PC speaker
	tmp = inb(0x61);
	if (tmp != (tmp | 3))
		outb(0x61, tmp | 3);
}
 
 //make it shutup
void nosound()
{
	uint8_t tmp = inb(0x61) & 0xFC;
	outb(0x61, tmp);
}
 
//Make a beep
void beep()
{
	play_sound(1000);
	timer_wait(5);
	nosound();
}

// TODO: system halts if song is empty
void melody(int melody[])
{
	
	int count = sizeof(melody)/sizeof(melody[0]);

	for(int i=0;i<count;i++)
	{
		if(melody[i]) 
			play_sound(melody[i]);
		else
			nosound();
		
		i++;
		timer_wait(melody[i]);
		nosound();
	}
}