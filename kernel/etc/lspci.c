#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <sys/io.h> // include inl() and outl()
#include <string.h>
#include "lspci.h"
#include "../drivers/display/tty.h"

const char* getVendorName(uint32_t vendorid) {
	return pci_vendor_list[vendorid];
}

// Read a 16-bit word from the PCI configuration space
uint32_t pciConfigReadWord(uint8_t bus, uint8_t slot, uint8_t func, uint8_t offset) {
	uint32_t address;
	uint32_t lbus  = (uint32_t)bus;
	uint32_t lslot = (uint32_t)slot;
	uint32_t lfunc = (uint32_t)func;
	uint32_t tmp = 0;

	// Create configuration address as per Figure 1
	address = (uint32_t)((lbus << 16) | (lslot << 11) |
						(lfunc << 8) | (offset & 0xFC) | ((uint32_t)0x80000000));
	// Write out the address
	outl(PCI_CONFIG_ADDRESS, address);
	// Read in the data
	// (offset & 2) * 8) = 0 will choose the first word of the 32-bit register
	tmp = (uint32_t)inl(PCI_CONFIG_DATA); //(uint32_t)((inl(PCI_CONFIG_DATA) >> ((offset & 2) * 8)) & 0xFFFF);
	//fprintf(stdout, "Raw: %x\n", tmp);
	// Check endianness
	uint16_t value = 0;
	if (*(uint8_t*)&value != 0x12) {
		// Big-endian, swap bytes
		tmp = (tmp >> 8) | (tmp << 8);
	}
	
	return tmp;
}

// Function to read from PCI configuration space
unsigned long pci_config_read(unsigned char bus, unsigned char slot,
															unsigned char func, unsigned char offset) {
	
	unsigned long address;
	unsigned long lbus = (unsigned long)bus;
	unsigned long lslot = (unsigned long)slot;
	unsigned long lfunc = (unsigned long)func;
	unsigned long tmp = 0;

	// Create configuration address
	address = (unsigned long)((lbus << 16) | (lslot << 11) |
														(lfunc << 8) | (offset & 0xFC) | ((unsigned long)0x80000000));

	// Write out the address
	outl(address, PCI_CONFIG_ADDRESS);

	// Read in the data
	// (offset & 2) * 8) = 0 will choose the first word of the 32 bits register
	tmp = inl(PCI_CONFIG_DATA);

	uint16_t value = 0;
	if (*(uint8_t*)&value != 0x12) {
		// Big-endian, swap bytes
		tmp = (tmp >> 8) | (tmp << 8);
	}

	return (tmp >> ((offset & 2) * 8)) & 0xFFFF;
}

/*
typedef struct {
	uint16_t vendor_id;
	uint16_t device_id;
	uint8_t class_code;
	uint8_t subclass;
	uint8_t prog_if;
	uint8_t revision;
	char* vendor_name;
} PCI_Device;
*/

/*
  Bus  0, device   3, function 0:
    Ethernet controller: PCI device 8086:100e
      PCI subsystem 1af4:1100
      IRQ 11, pin A
      BAR0: 32 bit memory at 0xfebc0000 [0xfebdffff].
      BAR1: I/O at 0xc000 [0xc03f].
      BAR6: 32 bit memory at 0xffffffffffffffff [0x0003fffe].
      id ""

*/

#define PCI_BAR_COUNT 6 // Number of Base Address Registers (BARs)
#define PCI_BAR_OFFSET 0x10 // Offset of the first Base Address Register (BAR)

// Constants for interpreting BARs
#define PCI_BAR_MMIO_TYPE 0x01 // Memory-mapped I/O (MMIO) type flag
#define PCI_BAR_MMIO_MASK (~0xF) // Mask for extracting MMIO base address
#define PCI_BAR_IO_MASK (~0x3) // Mask for extracting I/O port base address

void checkBus(uint16_t bus) {
	unsigned char slot, function;

	for (slot = 0; slot < 32; slot++) {
		for (function = 0; function < 8; function++) {
			PCI_Device device;
			
			// Read Vendor ID
			device.vendor_id = pci_config_read(bus, slot, function, PCI_VENDOR_ID);
			if (device.vendor_id == 0xFFFF) {
				continue;
			}
									
			// Read Device ID
			device.device_id = pci_config_read(bus, slot, function, PCI_DEVICE_ID);
			// Read Class Code
			device.class = pci_config_read(bus, slot, function, PCI_CLASS_CODE) >> 8;
                    // Read Subclass Code
			device.subclass = pci_config_read(bus, slot, function, PCI_SUBCLASS_CODE) >> 8;
			device.vendor_name = (char* )getVendorName(device.vendor_id);

			const char* device_class_str = device_type_list[device.class][device.subclass];
			
            // Read and interpret BARs
            uint32_t bars[PCI_BAR_COUNT];
            for (int bar = 0; bar < PCI_BAR_COUNT; bar++) {
							
							
							
                bars[bar] = pci_config_read(bus, slot, function, PCI_BAR_OFFSET + bar * sizeof(uint32_t));

                // Determine BAR type (MMIO or I/O port) and base address
                if (bars[bar] & PCI_BAR_MMIO_TYPE) {
                    // Memory-mapped I/O (MMIO) region
                    uint32_t base_address = bars[bar] & PCI_BAR_MMIO_MASK;
									if (base_address != 0x000) {
                    printf("Bus %02x, Device %02x, Function %d, BAR %d (MMIO): 0x%08x\n",
                                   bus, slot, function, bar, base_address);
									}
                } else {
                    // I/O port region
                    uint32_t base_address = bars[bar] & PCI_BAR_IO_MASK;
									if (base_address != 0x000) {
                    printf("Bus %02x, Device %02x, Function %d, BAR %d (I/O): 0x%04x\n",
                                   bus, slot, function, bar, base_address);
									}
                }
            }
			
			// Print PCI device information
			// (bus):(slot).(host): device_name
			// 00:1f.3 Audio device: Intel Corporation 200 Series PCH HD Audio
			printf("%02d:%02d.%d %s: %s, Device id: 0x%x\n",
							bus, slot, function,
							device_class_str, device.vendor_name,
							device.device_id);
		}
	}
}

void lspci(void) {

	uint16_t bus;
	
	for (bus = 0; bus < 255; bus++) {
		checkBus(bus);
	}
}