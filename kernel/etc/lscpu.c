#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "lscpu.h"
#include "../drivers/display/tty.h"

char* get_model_name() {
	unsigned int eax, ebx, ecx, edx;
	eax = 0x80000002; // Starting CPUID function for processor brand string
	asm volatile ("cpuid" : "=a" (eax), "=b" (ebx), "=c" (ecx), "=d" (edx) : "a" (eax));
	
	// Allocate a buffer to store the processor brand string
	char* brand_string = (char*)malloc(49); // 48 characters + null terminator

	// Copy the processor brand string characters into the buffer
	*(unsigned int*)(brand_string) = eax;
	*(unsigned int*)(brand_string + 4) = ebx;
	*(unsigned int*)(brand_string + 8) = ecx;
	*(unsigned int*)(brand_string + 12) = edx;

	eax = 0x80000003; // Continuing CPUID function for processor brand string
	asm volatile ("cpuid" : "=a" (eax), "=b" (ebx), "=c" (ecx), "=d" (edx) : "a" (eax));
	*(unsigned int*)(brand_string + 16) = eax;
	*(unsigned int*)(brand_string + 20) = ebx;
	*(unsigned int*)(brand_string + 24) = ecx;
	*(unsigned int*)(brand_string + 28) = edx;

	eax = 0x80000004; // Continuing CPUID function for processor brand string
	asm volatile ("cpuid" : "=a" (eax), "=b" (ebx), "=c" (ecx), "=d" (edx) : "a" (eax));
	*(unsigned int*)(brand_string + 32) = eax;
	*(unsigned int*)(brand_string + 36) = ebx;
	*(unsigned int*)(brand_string + 40) = ecx;
	*(unsigned int*)(brand_string + 44) = edx;

	// Null-terminate the string
	brand_string[48] = '\0';

	return brand_string;
}

char* get_cpu_architecture() {
	unsigned int eax, ebx, ecx, edx;
	eax = 0x00000001; // CPUID function for processor information
	asm volatile ("cpuid" : "=a" (eax), "=b" (ebx), "=c" (ecx), "=d" (edx) : "a" (eax));
    
	// Check the value of the CPUID bits to determine the architecture
	if (edx & (1 << 26)) {
		return "x86_64";
	} else {
		return "x86";
	}
}

char* get_cpu_op_modes() {
	unsigned int eax, ebx, ecx, edx;
	char *op_modes = (char *)malloc(50 * sizeof(char)); // Allocate memory for the string

	if (op_modes == NULL) {
		// Memory allocation failed
		return NULL;
	}

	// Initialize the string
	op_modes[0] = '\0';

	eax = 0x00000001; // CPUID function for processor information
	asm volatile ("cpuid" : "=a" (eax), "=b" (ebx), "=c" (ecx), "=d" (edx) : "a" (eax));

	// Check the value of the CPUID bits to determine the operating modes
	if (edx & (1 << 29)) {
		strcat(op_modes, "64-bit");
	} else {
		strcat(op_modes, "32-bit");
	}

	if (ecx & (1 << 0)) {
		if (strlen(op_modes) > 0) strcat(op_modes, ", ");
		strcat(op_modes, "IA-32e (x86-64)");
	}

	return op_modes;
}

int is_little_endian() {
	int num = 1;
	// Cast the integer to a pointer to a char
	// and dereference it to access the least significant byte
	char *byte = (char *)&num;
	// If the least significant byte is 1, the system is little-endian
	return (*byte == 1);
}

char* get_vendor_id() {
	static char vendor_id[13]; // Static buffer to store the vendor ID string

	uint32_t eax, ebx, ecx, edx;
	eax = 0x00000000; // CPUID function for processor information
	asm volatile ("cpuid" : "=b" (ebx), "=c" (ecx), "=d" (edx) : "a" (eax));

	// Copy the vendor ID bytes into the buffer
	*(uint32_t *)vendor_id = ebx;
	*(uint32_t *)(vendor_id + 4) = edx;
	*(uint32_t *)(vendor_id + 8) = ecx;

	// Null-terminate the string
	vendor_id[12] = '\0';

	return vendor_id;
}

void get_cpu_frequencies(uint32_t *max_freq_mhz, uint32_t *min_freq_mhz) {
	uint32_t eax, ebx, ecx, edx;

	// CPUID function to get maximum frequency
	eax = 0x16;  // CPUID function for maximum frequency
	asm volatile ("cpuid" : "=a" (eax), "=b" (ebx), "=c" (ecx), "=d" (edx) : "a" (eax));
	*max_freq_mhz = ebx;

	// CPUID function to get minimum frequency
	eax = 0x17;  // CPUID function for minimum frequency
	asm volatile ("cpuid" : "=a" (eax), "=b" (ebx), "=c" (ecx), "=d" (edx) : "a" (eax));
	*min_freq_mhz = ebx;
}

void get_cpu_count(int *sockets, int *cores_per_socket, int *threads_per_core) {
	uint32_t eax, ebx, ecx, edx;
	eax = 0x00000001; // CPUID function for processor information
	asm volatile ("cpuid" : "=b" (ebx), "=c" (ecx), "=d" (edx) : "a" (eax));

	*cores_per_socket = (ebx >> 16) & 0xFF; // Number of cores per CPU
	*threads_per_core = (ebx >> 24) & 0xFF; // Number of threads per core

	eax = 0x00000004; // CPUID function for number of processor cores
	asm volatile ("cpuid" : "=a" (eax), "=b" (ebx) : "a" (eax), "c" (1));
	*sockets = (eax >> 26) + 1; // Number of sockets
}

void get_cpu_address_sizes(uint32_t *physical_bits, uint32_t *virtual_bits) {
	uint32_t eax, ebx, ecx, edx;

	// CPUID function to get address sizes
	eax = 0x80000008;  // CPUID function for address sizes
	asm volatile ("cpuid" : "=a" (eax), "=b" (ebx), "=c" (ecx), "=d" (edx) : "a" (eax));
    
	// Extract address sizes from ecx register
	*physical_bits = (ecx >> 8) & 0xFF;
	*virtual_bits = ecx & 0xFF;
}

uint32_t get_cpu_model() {
	uint32_t model;

	// CPUID function to get processor information
	asm volatile (
		"cpuid"
		: "=a" (model)    // EAX contains the model information
		: "a" (1)
		: "ebx", "ecx", "edx");

	// Extract model information from bits 4-7 of EAX
	model = (model >> 4) & 0xF;

	return model;
}

uint32_t get_cpu_family() {
	uint32_t family;

	// CPUID function to get processor information
	asm volatile (
		"cpuid"
		: "=a" (family)    // EAX contains the family information
		: "a" (1)
		: "ebx", "ecx", "edx");

	// Extract family information from bits 8-11 and bits 20-27 of EAX
	family = (family >> 8) & 0xF;
	uint32_t extended_family = (family >> 20) & 0xFF;
	family += extended_family;

	return family;
}

void lscpu() {
	CPUdata cpu;

	cpu.architecture = get_cpu_architecture();
	if (cpu.architecture == NULL) {
		cpu.architecture = "(null)";
	}

	cpu.op_modes = get_cpu_op_modes();
	if (cpu.op_modes == NULL) {
		cpu.op_modes = "(null)";
	}

	get_cpu_address_sizes(&cpu.physical_bits, &cpu.virtual_bits);
	if (cpu.physical_bits == NULL) {
		cpu.physical_bits = 0;
	}
	if (cpu.virtual_bits == NULL) {
		cpu.virtual_bits = 0;
	}

	cpu.model_name = get_model_name();
	if (cpu.model_name == NULL) {
		cpu.model_name = "(null)";
	}

	cpu.byte_order = is_little_endian() ? "Little-endian" : "Big-endian";
	
	cpu.vendor_id = get_vendor_id();
	if (cpu.vendor_id == NULL) {
		cpu.vendor_id = "(null)";
	}

	cpu.model = get_cpu_model();
	if (cpu.model == NULL) {
		cpu.model = "(null)";
	}

	cpu.family = get_cpu_family();
	if (cpu.family == NULL) {
		cpu.family = "(null)";
	}

	get_cpu_count(&cpu.sockets, &cpu.cores_per_socket, &cpu.threads_per_core);
	cpu.total_cpus = cpu.sockets * cpu.cores_per_socket * cpu.threads_per_core;

	uint32_t max_freq_mhz, min_freq_mhz;
	get_cpu_frequencies(&max_freq_mhz, &min_freq_mhz);

	terminal_write("Architecture: %s\n", cpu.architecture);
	terminal_write("CPU op-mode(s): %s\n", NULL);
	terminal_write("Address sizes: %d bits physical, %d bits virtual\n", 0, 0);
	terminal_write("Byte Order: %s\n", cpu.byte_order);
	terminal_write("CPU(s): %d\n", 0);
	terminal_write("On-line CPU(s) list: %s\n", NULL);
	terminal_write("Vendor ID: %s\n", cpu.vendor_id);
	terminal_write("Model name: %s\n", cpu.model_name);
	//terminal_write("CPU family: %s\n", cpu.family);
	//terminal_write("Model: %s\n", cpu.model);
	terminal_write("Thread(s) per core: %d\n", cpu.threads_per_core);
	terminal_write("Core(s) per socket: %d\n", cpu.cores_per_socket);
	terminal_write("Socket(s): %d\n", cpu.sockets);
	//printf("Stepping:", );
	//printf("CPU(s) scaling MHz:", );
	terminal_write("CPU max MHz: %x\n", max_freq_mhz);
	terminal_write("CPU min MHz: %x\n", min_freq_mhz);
	//printf("BogoMIPS:", );
	//printf("Flags:", );
	//printf("Virtualization features:");
	//printf("Virtualization:", );
	//printf("Caches (sum of all):", );

}

/*
Real output from Linux:

Architecture:             x86_64
  CPU op-mode(s):         32-bit, 64-bit
  Address sizes:          39 bits physical, 48 bits virtual
  Byte Order:             Little Endian
CPU(s):                   12
  On-line CPU(s) list:    0-11
Vendor ID:                GenuineIntel
  Model name:             Intel(R) Core(TM) i7-8700K CPU @ 3.70GHz
    CPU family:           6
    Model:                158
    Thread(s) per core:   2
    Core(s) per socket:   6
    Socket(s):            1
    Stepping:             10
    CPU(s) scaling MHz:   63%
    CPU max MHz:          4700.0000
    CPU min MHz:          800.0000
    BogoMIPS:             7402.02
    Flags:                fpu vme de pse tsc msr pae mce cx8 apic sep mtrr pge mca cmov pat pse36 clflush dts acpi mmx fxsr sse sse2 ss ht tm 
                          pbe syscall nx pdpe1gb rdtscp lm constant_tsc art arch_perfmon pebs bts rep_good nopl xtopology nonstop_tsc cpuid ap
                          erfmperf pni pclmulqdq dtes64 monitor ds_cpl vmx smx est tm2 ssse3 sdbg fma cx16 xtpr pdcm pcid sse4_1 sse4_2 x2apic
                           movbe popcnt tsc_deadline_timer aes xsave avx f16c rdrand lahf_lm abm 3dnowprefetch cpuid_fault epb pti ssbd ibrs i
                          bpb stibp tpr_shadow flexpriority ept vpid ept_ad fsgsbase tsc_adjust bmi1 avx2 smep bmi2 erms invpcid mpx rdseed ad
                          x smap clflushopt intel_pt xsaveopt xsavec xgetbv1 xsaves dtherm ida arat pln pts hwp hwp_notify hwp_act_window hwp_
                          epp vnmi md_clear flush_l1d arch_capabilities
Virtualization features:  
  Virtualization:         VT-x
Caches (sum of all):      
  L1d:                    192 KiB (6 instances)
  L1i:                    192 KiB (6 instances)
  L2:                     1.5 MiB (6 instances)
  L3:                     12 MiB (1 instance)
NUMA:                     
  NUMA node(s):           1
  NUMA node0 CPU(s):      0-11
Vulnerabilities:          
  Gather data sampling:   Mitigation; Microcode
  Itlb multihit:          KVM: Mitigation: Split huge pages
  L1tf:                   Mitigation; PTE Inversion; VMX conditional cache flushes, SMT vulnerable
  Mds:                    Mitigation; Clear CPU buffers; SMT vulnerable
  Meltdown:               Mitigation; PTI
  Mmio stale data:        Mitigation; Clear CPU buffers; SMT vulnerable
  Reg file data sampling: Not affected
  Retbleed:               Mitigation; IBRS
  Spec rstack overflow:   Not affected
  Spec store bypass:      Mitigation; Speculative Store Bypass disabled via prctl
  Spectre v1:             Mitigation; usercopy/swapgs barriers and __user pointer sanitization
  Spectre v2:             Mitigation; IBRS; IBPB conditional; STIBP conditional; RSB filling; PBRSB-eIBRS Not affected; BHI Not affected
  Srbds:                  Mitigation; Microcode
  Tsx async abort:        Mitigation; TSX disabled
*/
