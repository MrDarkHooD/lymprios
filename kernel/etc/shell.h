#ifndef SHELL_H
#define SHELL_H

#define SHELL_BUFFER_SIZE 256

void shell_init(void);
char shell_input();

#endif