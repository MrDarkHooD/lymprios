#ifndef LSCPU_H
#define LSCPU_H

typedef struct {
	char* architecture;
	char *op_modes;
	uint32_t physical_bits; // Address size
	uint32_t virtual_bits; // Address size
	char* byte_order;
	int total_cpus;
	char* vendor_id;
	char* model_name;
	uint32_t family;
	uint32_t model;
	int threads_per_core;
	int cores_per_socket;
	int sockets;

} CPUdata;

void lscpu();

#endif
