#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <stddef.h>
#include <stdlib.h>

#include "drivers/display/tty.h"
#include "memory/gdt.h"
#include "kernel.h"
#include "interrupts/interrupts.h"
#include "drivers/input/keyboard/input.h"

/* Check if the compiler thinks you are targeting the wrong operating system. */
#if defined(__linux__)
#error "You are not using a cross-compiler, you will most certainly run into trouble"
#endif
 
/* This tutorial will only work for the 32-bit ix86 targets. */
#if !defined(__i386__)
#error "This kernel needs to be compiled with a ix86-elf compiler"
#endif

FILE *stderr = 0x3F8;
FILE *stdout = 0x3F8;

void kernel_main(void) 
{
	terminal_initialize();
	fprintf(stdout, "VGA initialized\n");
	idt_install();
	fprintf(stdout, "IDT loaded\n");
	isr_install();
	fprintf(stdout, "ISR loaded\n");
	irq_install();
	fprintf(stdout, "IRQ loaded\n");
	sys_enable_interrupts();
	fprintf(stdout, "Interrupts enabled\n");
	keyboard_install();
	fprintf(stdout, "Keyboard installed\n");
	
	int result = shell_init();

	switch (result) {
		case 0:
			fprintf(stderr, "Main shell returned with success message.\nRestarting shell...\n");
		default:
			fprintf(stderr, "Shell returned unknown error code, shutting down.\n");
			asm("cli");
			break;
	}
  for(;;) {}
  terminal_writestring("Kernel reached end for unknown reason.\n");
	return -1;
}
